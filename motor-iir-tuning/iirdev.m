%close all
clear all
clc

load('stopdata');

x = 0:1:length(data)-1;

f = zeros(1,length(data));

alpha = 0.85;
iirfilter = @(old,new) old*alpha+new*(1-alpha);

f(1) = data(1);
count = 0;
for i = 2:length(data)
    if(data(i) == data(i-1))
        count = count + 1;
    else
        count = 0;
    end
    if(count > 50)
        val = 0;
    else
        val = data(i);
    end
    f(i) = iirfilter(f(i-1),val);
end

range = 1:length(data);
%range = 1:200;
plot(x(range),data(range),x(range),f(range),x(range),abs(data(range)-f(range)))
title(alpha)
grid on