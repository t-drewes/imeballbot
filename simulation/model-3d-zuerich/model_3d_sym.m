clear all; close all;

syms theta_z(t) theta_x(t) theta_y(t) phi_x(t) phi_y(t) %% Minimal Coordinates
syms rB rW rA l mAW mB ThetaBi Theta_Wi;
syms A_Theta_AWx A_Theta_AWy A_Theta_AWz;
syms t;

syms theta_z_c theta_x_a theta_y_b phi_x_d phi_y_e oooooooooooo
syms alpha beta1 beta2 beta3

syms psi1(t) psi2(t) psi3(t)
syms psi1_dot(t) psi2_dot(t) psi3_dot(t);
syms phi_x_dot phi_y_dot theta_y_dot theta_x_dot theta_z_dot 
syms theta_x_ddot theta_y_ddot theta_z_ddot phi_x_ddot phi_y_ddot

syms a b c v w x y z zz

syms g L_Omega_B L_Theta_B A_Omega_A A_Theta_A A_Theta_W1 A_Theta_W2 A_Theta_W3 A_Omega_W1 A_Omega_W2 A_Omega_W3 I_Omega_B
syms T1(t) T2(t) T3(t)

%%%%%%%%%%%%%%%%%%% Variablen Definition %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% rB = Radius Ball
% rW = Radius Omniwheel
% rA = Radius Aufbau (halbe Kantenlaenge)
% mAW = Masse Aufbau mit Omniwheels
% ThetaBi = Traegeheitsmoment Ball i=x,y,z
% Theta_Wi = Traeheitsmoment Omniwheels
% A_Theta_AWx = Traegheitsmoment Aufbau w.r.t. X
% A_Theta_AWy = Traegheitsmoment Aufbau w.r.t. Y
% A_Theta_AWz = Traegheitsmoment Aufbau w.r.t. Z
% Psi1 = Winkelgeschwindigkeit Omniwheel
% Psi2 = Winkelgeschwindigkeit Omniwheel
% Psi3 = Winkelgeschwindigkeit Omniwheel
% Psi1_dot = Ableitung Psi1 nach t
% Psi2_dot = Ableitung Psi2 nach t
% Psi3_dot = Ableitung Psi3 nach t
% g = Schwerkraft
% m_A = Gewicht Aufbau
% l = Abstand COM Ball zu COM Aufbau
% m_B = Gewicht Ball
% I_Omega_B = Winkelgeschwindigkeit vom Ball
% rBP = Vektor von COM Ball zu Beruehrungspunkt Boden
% L_Omega_B = Winkelgeschwindigkeit Ball
% L_Theta_B = Traegheitsmoment Ball
% A_Omega_A = Winkelgeschwindigkeit Aufbau
% A_Theta_A = Traegheitsmoment Aufbau
% A_Theta_W1 = Traegheitsmoment Omniwheel 1
% A_Theta_W2 = Traegheitsmoment Omniwheel 2
% A_Theta_W3 = Traegheitsmoment Omniwheel 3
% A_Omega_W1 = Winkelgeschwindigkeit Omniwheel 1
% A_Omega_W2 = Winkelgeschwindigkeit Omniwheel 2
% A_Omega_W3 = Winkelgeschwindigkeit Omniwheel 3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

alpha %= deg2rad(42);
beta = deg2rad([0, 120, 240]);
beta1% = beta(1);
beta2 %= beta(2);
beta3 %= beta(3);

G = [0;  0; 9.81];

q = [theta_x, theta_y, theta_z, phi_x, phi_y];

% elementary rotation matrices
R_z = [cos(theta_z), -sin(theta_z), 0;
      sin(theta_z), cos(theta_z), 0;
      0, 0, 1];

R_y = [cos(theta_y), 0, sin(theta_y);
     0, 1, 0;
     -sin(theta_y), 0, cos(theta_y)];
 
R_x = [1, 0, 0;
      0, cos(theta_x), -sin(theta_x);
      0, sin(theta_x), cos(theta_x)];

R_IL = R_z;
R_LI = transpose(R_IL);

R_IA = R_IL * R_y * R_x;
R_AI = transpose(R_IA);

% p. 29, formula (4.1)
L_Omega_B = [diff(phi_x); diff(phi_y); 0];


% p. 29, formula (4.2)
A_omega_W1 = psi1_dot;
A_omega_W2 = psi2_dot;
A_omega_W3 = psi3_dot;

% p. 30, formula (4.3)
J = [1,0, -sin(theta_y);
     0, cos(theta_x), sin(theta_x) * cos(theta_y);
     0, -sin(theta_x), cos(theta_x) * cos(theta_y)];
ThetaDot = [diff(theta_x); diff(theta_y); diff(theta_z)];
A_Omega_A = J * ThetaDot;

% 4.1.4 Binding equations

% Absolute rotation of the omni-wheels, p. 30, figure (4.3)
A_MW1 = [cos(beta1) * sin(alpha), sin(alpha) * sin(beta1), -cos(alpha)];
A_MW2 = [cos(beta2) * sin(alpha), sin(alpha) * sin(beta2), -cos(alpha)];
A_MW3 = [cos(beta3) * sin(alpha), sin(alpha) * sin(beta3), -cos(alpha)];

%syms A_MW1

% p. 31, formula (4.4)
A_Omega_W1 = simplify(A_omega_W1 + dot(A_MW1, A_Omega_A(t)));
A_Omega_W2 = simplify(A_omega_W2 + dot(A_MW2, A_Omega_A(t)));
A_Omega_W3 = simplify(A_omega_W3 + dot(A_MW3, A_Omega_A(t)));

% p. 31, formula (4.5)
I_Omega_B = R_IL * L_Omega_B;
A_Omega_B = R_AI * I_Omega_B;
A_omega_B = simplify(A_Omega_B - A_Omega_A);

% p. 
A_Omega_W1 = simplify(A_Omega_W1);
A_Omega_W2 = simplify(A_Omega_W2);
A_Omega_W3 = simplify(A_Omega_W3);

% Translation of the ball, p. 32, formula (4.7)
r_BP = [0, 0, rB];
I_r_P_dot = simplify(cross(I_Omega_B, r_BP));

% p. 32, formula (4.8)
L_Theta_B = [ThetaBi, 0, 0;
          0, ThetaBi, 0;
          0, 0, ThetaBi];

% p. 32, formula (4.9)
A_Theta_AW = [A_Theta_AWx, 0, 0;
             0, A_Theta_AWy, 0;
             0, 0, A_Theta_AWz];

% Dependency on the rotation of the omni-wheels, p. 30, figure (4.3)
A_rPK1 = [rB * sin(alpha) * cos(beta1), rB * sin(alpha) * sin(beta1), rB * cos(alpha)];
A_rPK2 = [rB * sin(alpha) * cos(beta2), rB * sin(alpha) * sin(beta2), rB * cos(alpha)];
A_rPK3 = [rB * sin(alpha) * cos(beta3), rB * sin(alpha) * sin(beta3), rB * cos(alpha)];

A_d1 = [-sin(beta1), cos(beta1), 0];
A_d2 = [-sin(beta2), cos(beta2), 0];
A_d3 = [-sin(beta3), cos(beta3), 0];

TangentialSpeedW1 = dot(cross(A_omega_B(t), A_rPK1), A_d1) == A_omega_W1 * rW;
TangentialSpeedW2 = dot(cross(A_omega_B(t), A_rPK2), A_d2) == A_omega_W2 * rW;
TangentialSpeedW3 = dot(cross(A_omega_B(t), A_rPK3), A_d3) == A_omega_W3 * rW;

sol = solve([subs(TangentialSpeedW1(t),psi1_dot(t),a); subs(TangentialSpeedW2(t),psi2_dot(t),b); subs(TangentialSpeedW3(t),psi3_dot(t),c)], [a,b,c]);

psi1_dot = simplify(sol.a);
psi2_dot = simplify(sol.b);
psi3_dot = simplify(sol.c);

% p. 34, formula (4.12)
T_B = simplify(1/2 * mB * dot(I_r_P_dot(t), I_r_P_dot(t)) + 1/2 * L_Omega_B' * L_Theta_B * L_Omega_B); 
V_B = 0;

% p. 34, formula (4.13)
A_rPSA = [0; 0; 1];
T_AW = simplify(1/2 * mAW * dot(I_r_P_dot(t), I_r_P_dot(t)) + mAW * dot((R_AI(t) * I_r_P_dot(t)'), cross(A_Omega_A(t), A_rPSA)) + ...
    1/2*A_Omega_A' * A_Theta_AW * A_Omega_A);

% p. 34, formula (4.14)
V_AW = -mAW * G' * R_IA * A_rPSA;

% p. 35, formula (4.15)
T_W1 = simplify(1/2 * A_Theta_W1 * A_Omega_W1(t)^2);
T_W2 = simplify(1/2 * A_Theta_W2 * A_Omega_W2(t)^2);
T_W3 = simplify(1/2 * A_Theta_W3 * A_Omega_W3(t)^2);

T = T_B + T_AW + T_W1 + T_W2 + T_W3;
V = V_AW + V_B;

% Ableitungen
V_AW_dot = diff(subs(V_AW, theta_y, zz), zz);

JT1 = A_omega_W1(t);
JT2 = A_omega_W2(t);
JT3 = A_omega_W3(t);

JTC = A_Omega_A(t);

% p. 35, formula (4.17)
TC1(t) = A_MW1 * (-T1(t));
TC2(t) = A_MW2 * (-T2(t));
TC3(t) = A_MW3 * (-T3(t));

f_NP = simplify(JT1 * T1(t) + JT2 * T2(t) + JT3 * T3(t) + (TC1(t) + TC2(t) + TC3(t)) * JTC);

% 4.2.3 Equations of motion

% L1

% L2
T %%%TODO: Comments
% jacobian only works with variables and not with functions
% so it is necessary to substitute all functions with varibales
T = subs(T, {diff(theta_x(t), t), diff(theta_y(t), t), diff(theta_z(t), t), diff(phi_x(t), t), diff(phi_y(t), t)}, {theta_x_dot, theta_y_dot, theta_z_dot, phi_x_dot, phi_y_dot});
%T = subs(T, {theta_z, theta_y}, {z, y});
T = subs(T, {theta_x(t), theta_y(t), theta_z(t), phi_x(t), phi_y(t)}, {x, y, z, v, w});
T_q_1 = diff(T, x);
T_q_2 = diff(T, y);
T_q_3 = diff(T, z);
T_q_4 = diff(T, v);
T_q_5 = diff(T, w);
%T_q = simplify(jacobian(T, [x, y, z, v, w]));
T_q_dot_1 = diff(T, theta_x_dot);
T_q_dot_2 = diff(T, theta_y_dot);
T_q_dot_3 = diff(T, theta_z_dot);
T_q_dot_4 = diff(T, phi_x_dot);
T_q_dot_5 = diff(T, phi_y_dot);

T_q = [T_q_1; T_q_2; T_q_3; T_q_4; T_q_5];
T_q_dot = [T_q_dot_1; T_q_dot_2; T_q_dot_3; T_q_dot_4; T_q_dot_5];

%T_q_dot = simplify(jacobian(T, [theta_x_dot, theta_y_dot, theta_z_dot, phi_x_dot, phi_y_dot]));

% L3, p. 35, formula (4.19)
V
V(t) = subs(V(t), {theta_x(t), theta_y(t), theta_z(t), phi_x(t), phi_y(t)}, {x, y, z, v, w});
V_q1 = diff(V, x);
V_q2 = diff(V, y);
V_q3 = diff(V, z);
V_q4 = diff(V, v);
V_q5 = diff(V, w);

V_q = [V_q1; V_q2; V_q3; V_q4; V_q5];

%jacobian(V, [x y])
%T_q = subs(T_q, {x, y, z, v, w}, {theta_x(t), theta_y(t), theta_z(t), phi_x(t), phi_y(t)});
%T_q_dot = subs(T_q_dot, {theta_x_dot, theta_y_dot, theta_z_dot, phi_x_dot, phi_y_dot}, {diff(theta_x(t), t), diff(theta_y(t), t), diff(theta_z(t), t), diff(phi_x(t), t), diff(phi_y(t), t)});


% to get the time derivative first all energies and forces need to be
% summed up. Then the substituted variables are changed back to functions
all_energies = simplify(-T_q + T_q_dot + V);
all_energies = subs(all_energies, {x, y, z, v, w, theta_x_dot, theta_y_dot, theta_z_dot, phi_x_dot, phi_y_dot}, {theta_x(t), theta_y(t), theta_z(t), phi_x(t), phi_y(t),diff(theta_x(t), t), diff(theta_y(t), t), diff(theta_z(t), t), diff(phi_x(t), t), diff(phi_y(t), t)});
all_time = diff(all_energies, t) - f_NP;
all_time = simplify(all_time);
 
string = char(all_time);
file = fopen('bla.txt', 'w');
fprintf(file, '%s', string);

all_time = subs(all_time, {diff(theta_x(t),t,t), diff(theta_y(t),t,t), diff(theta_z(t),t,t), diff(phi_x(t),t,t), diff(phi_y(t),t,t)}, {theta_x_ddot, theta_y_ddot, theta_z_ddot, phi_x_ddot, phi_y_ddot});
all_time = subs(all_time, {diff(theta_x(t), t), diff(theta_y(t), t), diff(theta_z(t), t), diff(phi_x(t), t), diff(phi_y(t), t)}, {theta_x_dot, theta_y_dot, theta_z_dot, phi_x_dot, phi_y_dot});
all_time = subs(all_time, {theta_x(t), theta_y(t), theta_z(t), phi_x(t), phi_y(t)}, {x, y, z, v, w});

all_time = subs(all_time, {alpha, beta1, beta2, beta3}, {deg2rad(42), deg2rad(0), deg2rad(120), deg2rad(240)});
all_time = subs(all_time, {rB, rW, rA, l, mAW, mB}, {0.122, 0.05, 0.15, 0.19, 4.5, 0.588});
all_time = subs(all_time, {ThetaBi, A_Theta_AWx, A_Theta_AWy, A_Theta_AWz, A_Theta_W1, A_Theta_W2, A_Theta_W3}, {0.005835, 0.057188, 0.057188, 0.0675, 0.000474, 0.000474, 0.000474});
varList = [theta_x_ddot, theta_y_ddot, theta_z_ddot, phi_x_ddot, phi_y_ddot];
beep;
solution = solve(all_time, varList, 'IgnoreAnalyticConstraints', true);%, 'ReturnConditions', true);
beep;

string = char(solution);
file = fopen('sol.txt', 'w');
fprintf(file, '%s', string);
% 
% all_time = subs(all_time, {diff(theta_x(t),t,t), diff(theta_y(t),t,t), diff(theta_z(t),t,t), diff(phi_x(t),t,t), diff(phi_y(t),t,t)}, {theta_x_ddot, theta_y_ddot, theta_z_ddot, phi_x_ddot, phi_y_ddot});
% all_time = subs(all_time, {diff(theta_x(t), t), diff(theta_y(t), t), diff(theta_z(t), t), diff(phi_x(t), t), diff(phi_y(t), t)}, {theta_x_dot, theta_y_dot, theta_z_dot, phi_x_dot, phi_y_dot});
% all_time = subs(all_time, {theta_x(t), theta_y(t), theta_z(t), phi_x(t), phi_y(t)}, {x, y, z, v, w});
% all_time = simplify(all_time);
% 
% string = char(all_time');
% file = fopen('bla2.txt', 'w');
% fprintf(file, '%s', string);
% 
% all_time = subs(all_time, {alpha, beta1, beta2, beta3}, {deg2rad(42), deg2rad(0), deg2rad(120), deg2rad(240)});
% all_time = subs(all_time, {rB, rW, rA, l, mAW, mB}, {0.122, 0.05, 0.15, 0.19, 4.5, 0.588});
% all_time = subs(all_time, {ThetaBi, A_Theta_AWx, A_Theta_AWy, A_Theta_AWz, A_Theta_W1, A_Theta_W2, A_Theta_W3}, {0.005835, 0.057188, 0.057188, 0.0675, 0.000474, 0.000474, 0.000474});
% 
% string = char(all_time');
% file = fopen('bla3.txt', 'w');
% fprintf(file, '%s', string);
% 
% EQ = all_time; %== 0;
% EQ = EQ';
% 
% string = char(all_time');
% file = fopen('bla4.txt', 'w');
% fprintf(file, '%s', string);
% 
% EQ1 = all_time(1);
% EQ2 = all_time(2);
% EQ3 = all_time(3);
% EQ4 = all_time(4);
% EQ5 = all_time(5);
% 
% all_time = all_time == 0;
% 
% %%%%%%% Alternative zu solve finden
% EqnList = [EQ1, EQ2, EQ3, EQ4, EQ5];
% varList = [theta_x_ddot, theta_y_ddot]%, theta_z_ddot, phi_x_ddot, phi_y_ddot];
% solution1 = solve(EqnList, theta_x_ddot, 'IgnoreAnalyticConstraints', true, 'ReturnConditions', true);
% solution2 = solve(EqnList, theta_y_ddot, 'IgnoreAnalyticConstraints', true, 'ReturnConditions', true);
% solution3 = solve(EqnList, theta_z_ddot, 'IgnoreAnalyticConstraints', true, 'ReturnConditions', true);
% solution4 = solve(EqnList, phi_x_ddot, 'IgnoreAnalyticConstraints', true, 'ReturnConditions', true);
% solution5 = solve(EqnList, phi_y_ddot, 'IgnoreAnalyticConstraints', true, 'ReturnConditions', true);
% 
% blaSol = solve(all_time, varList, 'IgnoreAnalyticConstraints', true);
% 
% solution1.theta_x_ddot = subs(solution1.theta_x_ddot, {theta_y_ddot, theta_z_ddot, phi_x_ddot, phi_y_ddot}, {solution2.theta_y_ddot, solution3.theta_z_ddot, solution4.phi_x_ddot, solution5.phi_y_ddot})

% solution1.theta_x_ddot = simplify(solution1.theta_x_ddot);
% solution2.theta_y_ddot = simplify(solution2.theta_y_ddot);
% solution3.theta_z_ddot = simplify(solution3.theta_z_ddot);
% solution4.phi_x_ddot = simplify(solution4.phi_x_ddot);
% solution5.phi_y_ddot = simplify(solution5.phi_y_ddot);
% 
% string = char(solution1.theta_x_ddot);
% file = fopen('solution1.txt', 'w');
% fprintf(file, '%s', string);
% 
% string = char(solution1.conditions);
% file = fopen('sol1cond.txt', 'w');
% fprintf(file, '%s', string);
% 
% string = char(solution2.theta_y_ddot);
% file = fopen('solution2.txt', 'w');
% fprintf(file, '%s', string);
% 
% string = char(solution2.conditions);
% file = fopen('sol2cond.txt', 'w');
% fprintf(file, '%s', string);
% 
% string = char(solution3.theta_z_ddot);
% file = fopen('solution3.txt', 'w');
% fprintf(file, '%s', string);
% 
% string = char(solution3.conditions);
% file = fopen('sol3cond.txt', 'w');
% fprintf(file, '%s', string);
% 
% string = char(solution4.phi_x_ddot);
% file = fopen('solution4.txt', 'w');
% fprintf(file, '%s', string);
% 
% string = char(solution4.conditions);
% file = fopen('sol4cond.txt', 'w');
% fprintf(file, '%s', string);
% 
% string = char(solution5.phi_y_ddot);
% file = fopen('solution5.txt', 'w');
% fprintf(file, '%s', string);
% 
% string = char(solution5.conditions);
% file = fopen('sol5cond.txt', 'w');
% fprintf(file, '%s', string);
beep;