% see Zuerich paper
syms r_K r_W Tx m_K m_A m_W l Theta_K Theta_W Theta_A g;
syms theta_x theta_dot_x theta_2dot_x phi_2dot_x;

% non-potential forces = external forces on system, i.e. motor torques
% p. 11, formula (2.17)
f_NP_yz1 = [(r_K/r_W) * Tx; -(1 + r_K/r_W)*Tx];
% p. 11, formula (2.18)
f_NP_yz2 = [0; Tx];

f_NP = f_NP_yz1 + f_NP_yz2;
f_NP = simplify(f_NP);

% define helper variables
m_tot = m_K + m_A + m_W;
r_tot = r_K + r_W;
gamma = l * m_A + (r_K + r_W) * m_W;

% masses and inertias, p. 12, formula (2.22)
Mx = [m_tot*r_K^2+Theta_K + (r_K/r_W)^2*Theta_W -(r_K/r_W^2)*r_tot*Theta_W+gamma*r_K*cos(theta_x);
    -(r_K/r_W^2)*r_tot*Theta_W+gamma*r_K*cos(theta_x) (r_tot^2/r_W^2)*Theta_W+Theta_A+m_A*l^2+m_W*r_tot^2];

% coriolis forces, p. 12, formula (2.23)
Cx = [-r_K*gamma*sin(theta_x)*theta_dot_x^2;
    0];

% gravitational forces, p. 12, formula (2.24)
Gx = [0;
    -g*sin(theta_x)*gamma];

% put everything together according to p.12, formula (2.21)
q_2dot = simplify(Mx^-1 * (f_NP - Cx - Gx))
