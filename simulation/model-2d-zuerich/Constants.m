% Constants
g = 9.81; % Gravitationsbeschleunigung

r_W = 0.05; % Radius Rad
r_K = 0.1114; % Radius Ball
r_A = 0.15; % Radius Aufbau

m_K = 0.490; % Masse Ball
m_A = 0.997; % Masse Aufbau
l = 0.2; % Abstand m_K zu m_A (Schwerpunkt)
m_W_real = 0.343; % Masse echtes Rad
m_W = m_W_real + 0.402; % Masse virtuelles Rad
a_W = 42; % Winkel Rad in grad

% Values derived from Constants
alpha = deg2rad(a_W); % Winkel Rad in rad

Theta_W_xy = 0.5 * m_W * r_W^2; % Traegheitsmoment Rad (angen�hert als d�nne Scheibe)
Theta_A_xy = 2/3 * m_A * r_A^2; % Traegheitsmoment Aufbau
Theta_K = 2/3 * m_K * r_K^2; % Traegheitsmoment Ball (angen�hert als Hohlkugel)
Theta_W = 0.5 * m_W * r_W^2; % Traegheitsmoment Rad
Theta_A = 0.5 * m_A * (r_A^2 + l^2); % Traegheitsmoment Aufbau
