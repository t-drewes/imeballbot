/*
 * MotorDriver.c
 *
 *  Created on: May 12, 2016
 *      Author: Morten Mey
 */

#include "MotorDriver.h"
#include <DAVE.h>

static const uint32_t MAX_SPEED = 10000U;

static unsigned char normalizeSpeed(double speed, uint32_t* resSpeed){
	unsigned char reverse = 0;
	if(speed < 0){
		speed = -speed;
		reverse = 1;
	}

	if(speed > 1.0){
		XMC_DEBUG("Speed > MAX_SPEED (1.0)");
		speed = 1.0;
	}

	*resSpeed = speed * MAX_SPEED;
	return reverse;
}

static void setPin(const DIGITAL_IO_t* handle, uint8_t val){
	if(val){
		DIGITAL_IO_SetOutputHigh(handle);
	}else{
		DIGITAL_IO_SetOutputLow(handle);
	}
}

void setEnabled(uint8_t power){
	setPin(&ND2, power);
}

void setM1Speed(double speed){
	uint32_t cycle;
	unsigned char reverse = normalizeSpeed(speed, &cycle);
	PWM_CCU8_SetDutyCycleSymmetric(&M1PWM, XMC_CCU8_SLICE_COMPARE_CHANNEL_1, MAX_SPEED - cycle);
	setPin(&M1DIR, reverse);
}


uint32_t getM1CurrentMilliamps(){
	return 0;
}

uint8_t getFault(){
	return !DIGITAL_IO_GetInput(&NSF);
}
