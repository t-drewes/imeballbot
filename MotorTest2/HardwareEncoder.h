/*
 * HardwareEncoder.h
 *
 *  Created on: Jul 7, 2016
 *      Author: Morten Mey
 */

#ifndef HARDWAREENCODER_H_
#define HARDWAREENCODER_H_

#include <XMCLib/inc/xmc_posif.h>
#include <XMCLib/inc/xmc_ccu4.h>

void EncoderInit();

uint16_t getRawCount();
int32_t getDirection();
int32_t getRawPeriod();
double getWheelPosition();
int32_t getWheelRevolutions();
double getRawWheelSpeed();
double getWheelSpeed();

void updateSpeedFilter(); //call every ~1ms

#define BUFFER_SIZE 5000
extern bool startLog;
extern double buffer[BUFFER_SIZE];
extern double buffer2[BUFFER_SIZE];

#endif /* HARDWAREENCODER_H_ */
