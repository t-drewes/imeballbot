/*
 * main.c
 *
 *  Created on: 2016 May 12 08:50:12
 *  Author: Morten Mey
 */




#include <DAVE.h>                 //Declarations from DAVE Code Generation (includes SFR declaration)
#include "PrintfRedirect.h"
#include <stdio.h>
#include "MotorDriver.h"
#include "HardwareEncoder.h"
#include "MotorContoller.h"

uint32_t counter = 0;

struct MotorController_t controller = {
		.Ta = 0.001,
		.Kp = 15,
		.Ki = 20,
		.Kd = 0.05
};

void callback(void* data){
	static double speed = 0.0;

	counter++;

	if(counter == 100){
		counter = 0;
		if(controller.targetSpeed > 1.0){
			setTargetSpeed(&controller, 0.0);
		}else{
			setTargetSpeed(&controller, 5.0);
		}
		//speed = 0.25;
		//startLog = true;
	}/*else if(counter >= 110){
		speed = 0;

		static uint32_t idx = 0;
		if(idx < BUFFER_SIZE){
			myPrintf("%f, %f\n", buffer[idx], buffer2[idx]);
			idx++;
		}else if (idx == BUFFER_SIZE){
			myPrintf("\nDone\n");
			idx++;
		}
	}

	setM1Speed(speed);
*/
	//myPrintf("Pos: %f, Rots: %i Dir: %i, Speed: %f\n", getWheelPosition(), getWheelRevolutions(), getDirection(), getWheelSpeed());
	myPrintf("Speed: %f, Out:%f, Err: %f, SummErr: %f\n", getWheelSpeed(), controller.currentOutput, controller.lastError, controller.summedError);

	if(getFault()){
		myPrintf("FAULT\n");
	}

}

void fastCallback(void* data){
	updateSpeedFilter();
	updateControl(&controller);
}

/**

 * @brief main() - Application entry point
 *
 * <b>Details of function</b><br>
 * This routine is the application entry point. It is invoked by the device startup code. It is responsible for
 * invoking the APP initialization dispatcher routine - DAVE_Init() and hosting the place-holder for user application
 * code.
 */

int main(void)
{
  DAVE_STATUS_t status;

  status = DAVE_Init();           /* Initialization of DAVE APPs  */

  EncoderInit();

  initController(&controller);

  if(status == DAVE_STATUS_FAILURE)
  {
    /* Placeholder for error handler code. The while loop below can be replaced with an user error handler. */
    XMC_DEBUG("DAVE APPs initialization failed\n");

    while(1U)
    {

    }
  }

  /* Placeholder for user application code. The while loop below can be replaced with user application code. */
  uint32_t timer = SYSTIMER_CreateTimer(100*1000, SYSTIMER_MODE_PERIODIC, callback, NULL);
  SYSTIMER_StartTimer(timer);

  timer = SYSTIMER_CreateTimer(1000, SYSTIMER_MODE_PERIODIC, fastCallback, NULL);
  SYSTIMER_StartTimer(timer);

  setEnabled(1);
  myPrintf("STARTING\n");
  while(1U)
  {
  }
}
