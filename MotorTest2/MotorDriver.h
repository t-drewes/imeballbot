/*
 * MotorDriver.h
 *
 *  Created on: May 12, 2016
 *      Author: Morten Mey
 */

#ifndef MOTORDRIVER_H_
#define MOTORDRIVER_H_

#include<stdint.h>

/**
 * Call Dave_Init() before using the MotorDriver.
 *	setEnable(1) = turn On
 *	setEnable(0) = tunr Off
 */
void setEnabled(uint8_t power);

/**
 * -1.0 <= speed <= 1.0
 */
void setM1Speed(double speed);

/**
 * Not currently implemented.
 */
uint32_t getM1CurrentMilliamps();

/**
 * return value = 0 everything ok
 * return value != 0 FAULT
 */
uint8_t getFault();

#endif /* MOTORDRIVER_H_ */
