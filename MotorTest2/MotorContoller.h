/*
 * MotorContoller.h
 *
 *  Created on: Jul 14, 2016
 *      Author: Morten Mey
 */

#ifndef MOTORCONTOLLER_H_
#define MOTORCONTOLLER_H_

#define MAX_SUMMED_ERROR 0.0

struct MotorController_t{
	double targetSpeed; //in wheel rotations per second
	double lastError;
	double summedError;
	double currentOutput; //current value send to motor
	double Kp; //pid controller
	double Ki;
	double Kd;
	double Ta; //intervall time in seconds
};

void updateControl(struct MotorController_t* cont);
void setTargetSpeed(struct MotorController_t* cont, double speedInWheelRotationsPerSecond);
void initController(struct MotorController_t* cont);

#endif /* MOTORCONTOLLER_H_ */
