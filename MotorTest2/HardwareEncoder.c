/*
 * HardwareEncoder.c
 *
 *  Created on: Jul 7, 2016
 *      Author: Morten Mey
 */

#include "HardwareEncoder.h"
#include <XMCLib/inc/xmc_eru.h>
#include <math.h>

#define GEAR_RATIO 3.71
#define TICKS_PER_MOTOR_REVOLUTION (1024*4)
#define TICKS_PER_WHEEL_REVOLUTION (TICKS_PER_MOTOR_REVOLUTION * GEAR_RATIO)

static int32_t revolutions;

void CCU41_1_IRQHandler(){
	if(getDirection() > 0){
		revolutions++;
	}else{
		revolutions--;
	}
}


/*void CCU41_0_IRQHandler(){
	//bool reg1avail = CCU41_CC43->CV[1] & (1 << 20); //FFL full flag, set if new value was captured
	//static eval = false;
	//if(eval){
	//	int32_t reg0 = XMC_CCU4_SLICE_GetCaptureRegisterValue(CCU41_CC43, 0);
	//	int32_t reg1 = XMC_CCU4_SLICE_GetCaptureRegisterValue(CCU41_CC43, 1);
	//	speed =  reg1 - reg0;
	//}
	//eval = !eval;
	period = XMC_CCU4_SLICE_GetCaptureRegisterValue(CCU41_CC43, 1) & 0xFFFF;
}*/

static XMC_POSIF_CONFIG_t posifConfig = {
		.mode = XMC_POSIF_MODE_QD,
		.input0 = XMC_POSIF_INPUT_PORT_B,
		.input1 = XMC_POSIF_INPUT_PORT_B,
		.filter = XMC_POSIF_FILTER_DISABLED
};

static XMC_POSIF_QD_CONFIG_t posifQDConfig = {
	.mode = XMC_POSIF_QD_MODE_QUADRATURE,
	.phase_a = XMC_POSIF_INPUT_ACTIVE_LEVEL_HIGH,
	.phase_b = XMC_POSIF_INPUT_ACTIVE_LEVEL_HIGH,
	.phase_leader = 0,
	.index = XMC_POSIF_QD_INDEX_GENERATION_NEVER
};

static XMC_CCU4_SLICE_EVENT_CONFIG_t slice0ev0 = {
		.duration = XMC_CCU4_SLICE_EVENT_FILTER_DISABLED,
		.edge = XMC_CCU4_SLICE_EVENT_EDGE_SENSITIVITY_RISING_EDGE,
		.level = XMC_CCU4_SLICE_EVENT_LEVEL_SENSITIVITY_ACTIVE_HIGH,
		.mapped_input = CCU41_IN0_POSIF1_OUT0
};
static XMC_CCU4_SLICE_EVENT_CONFIG_t slice0ev1 = {
		.duration = XMC_CCU4_SLICE_EVENT_FILTER_DISABLED,
		.edge = XMC_CCU4_SLICE_EVENT_EDGE_SENSITIVITY_NONE,
		.level = XMC_CCU4_SLICE_EVENT_LEVEL_SENSITIVITY_ACTIVE_HIGH,
		.mapped_input = CCU41_IN0_POSIF1_OUT1
};

static XMC_CCU4_SLICE_EVENT_CONFIG_t slice2ev0 = {
		.duration = XMC_CCU4_SLICE_EVENT_FILTER_DISABLED,
		.edge = XMC_CCU4_SLICE_EVENT_EDGE_SENSITIVITY_RISING_EDGE,
		.level = XMC_CCU4_SLICE_EVENT_LEVEL_SENSITIVITY_ACTIVE_HIGH,
		.mapped_input = CCU41_IN2_POSIF1_OUT2
};

static XMC_CCU4_SLICE_EVENT_CONFIG_t slice3ev0 = {
		.duration = XMC_CCU4_SLICE_EVENT_FILTER_DISABLED,
		.edge = XMC_CCU4_SLICE_EVENT_EDGE_SENSITIVITY_DUAL_EDGE,
		.level = XMC_CCU4_SLICE_EVENT_LEVEL_SENSITIVITY_ACTIVE_HIGH,
		.mapped_input = CCU41_IN3_CCU41_ST2
};
static XMC_CCU4_SLICE_EVENT_CONFIG_t slice3ev1 = {
		.duration = XMC_CCU4_SLICE_EVENT_FILTER_DISABLED,
		.edge = XMC_CCU4_SLICE_EVENT_EDGE_SENSITIVITY_RISING_EDGE,
		.level = XMC_CCU4_SLICE_EVENT_LEVEL_SENSITIVITY_ACTIVE_HIGH,
		.mapped_input = CCU41_IN3_POSIF1_OUT5
};

static XMC_CCU4_SLICE_CAPTURE_CONFIG_t slice3CaptureConf = {
	   .fifo_enable	= false,
	   .timer_clear_mode = XMC_CCU4_SLICE_TIMER_CLEAR_MODE_ALWAYS,
	   .same_event = false,
	   .ignore_full_flag = true,
	   .prescaler_mode = XMC_CCU4_SLICE_PRESCALER_MODE_NORMAL,
	   .prescaler_initval = (uint32_t) 7, /* in this case, prescaler = 2^7  */
	   .float_limit = (uint32_t) 0,
	   .timer_concatenation	= (uint32_t) 0
};

void EncoderInit(){
//using posif1
//using ccu41

	XMC_POSIF_Init(POSIF1, &posifConfig);
	XMC_POSIF_QD_Init(POSIF1, &posifQDConfig);

	XMC_CCU4_Init(CCU41, XMC_CCU4_SLICE_MCMS_ACTION_TRANSFER_PR_CR);
	XMC_CCU4_StartPrescaler(CCU41);
	XMC_CCU4_SetModuleClock(CCU41, XMC_CCU4_CLOCK_SCU);

	//XMC_CCU4_SLICE_SetInput(CCU41_CC40, XMC_CCU4_SLICE_EVENT_0, XMC_CCU4_SLICE_INPUT_E); // POSIF1.OUT0
	//XMC_CCU4_SLICE_SetInput(CCU41_CC40, XMC_CCU4_SLICE_EVENT_1, XMC_CCU4_SLICE_INPUT_F); //POSIF1.OUT1
	XMC_CCU4_SLICE_ConfigureEvent(CCU41_CC40, XMC_CCU4_SLICE_EVENT_0, &slice0ev0);
	XMC_CCU4_SLICE_ConfigureEvent(CCU41_CC40, XMC_CCU4_SLICE_EVENT_1, &slice0ev1);
	XMC_CCU4_SLICE_CountConfig(CCU41_CC40, XMC_CCU4_SLICE_EVENT_0);
	XMC_CCU4_SLICE_DirectionConfig(CCU41_CC40, XMC_CCU4_SLICE_EVENT_1);
	XMC_CCU4_SLICE_SetTimerCountingMode(CCU41_CC40, XMC_CCU4_SLICE_TIMER_COUNT_MODE_EA);
	XMC_CCU4_SLICE_SetTimerCompareMatch(CCU41_CC40, (uint16_t) TICKS_PER_WHEEL_REVOLUTION);
	XMC_CCU4_SLICE_SetTimerPeriodMatch(CCU41_CC40, (uint16_t) TICKS_PER_WHEEL_REVOLUTION);

	XMC_CCU4_SLICE_EnableEvent(CCU41_CC40, XMC_CCU4_SLICE_IRQ_ID_PERIOD_MATCH);
	XMC_CCU4_SLICE_SetInterruptNode(CCU41_CC40, XMC_CCU4_SLICE_IRQ_ID_PERIOD_MATCH, XMC_CCU4_SLICE_SR_ID_1);
	XMC_CCU4_SLICE_EnableEvent(CCU41_CC40, XMC_CCU4_SLICE_IRQ_ID_ONE_MATCH);
	XMC_CCU4_SLICE_SetInterruptNode(CCU41_CC40, XMC_CCU4_SLICE_IRQ_ID_ONE_MATCH, XMC_CCU4_SLICE_SR_ID_1);
	NVIC_EnableIRQ(CCU41_1_IRQn);

	XMC_CCU4_EnableShadowTransfer(CCU41, XMC_CCU4_SHADOW_TRANSFER_SLICE_0);

	XMC_CCU4_SLICE_ConfigureEvent(CCU41_CC42, XMC_CCU4_SLICE_EVENT_0, &slice2ev0);
	XMC_CCU4_SLICE_CountConfig(CCU41_CC42, XMC_CCU4_SLICE_EVENT_0);
	XMC_CCU4_SLICE_SetTimerCountingMode(CCU41_CC42, XMC_CCU4_SLICE_TIMER_COUNT_MODE_EA);
	XMC_CCU4_SLICE_SetTimerCompareMatch(CCU41_CC42, 127);
	XMC_CCU4_SLICE_SetTimerPeriodMatch(CCU41_CC42, 255);

	XMC_CCU4_EnableShadowTransfer(CCU41, XMC_CCU4_SHADOW_TRANSFER_SLICE_2);

	XMC_CCU4_SLICE_ConfigureEvent(CCU41_CC43, XMC_CCU4_SLICE_EVENT_0, &slice3ev0);
	//XMC_CCU4_SLICE_ConfigureEvent(CCU41_CC43, XMC_CCU4_SLICE_EVENT_1, &slice3ev1);
	//XMC_CCU4_SLICE_StartConfig(CCU41_CC43, XMC_CCU4_SLICE_EVENT_1, XMC_CCU4_SLICE_START_MODE_TIMER_START_CLEAR);
	XMC_CCU4_SLICE_CaptureInit(CCU41_CC43, &slice3CaptureConf);
	XMC_CCU4_SLICE_Capture0Config(CCU41_CC43, XMC_CCU4_SLICE_EVENT_0);
	//CCU41_CC43->TC |= CCU4_CC4_TC_CCS_Msk;
	XMC_CCU4_SLICE_SetTimerPeriodMatch(CCU41_CC43, 0xFFFF);

	//XMC_CCU4_SLICE_EnableEvent(CCU41_CC43, XMC_CCU4_SLICE_IRQ_ID_EVENT0);
	//XMC_CCU4_SLICE_SetInterruptNode(CCU41_CC43, XMC_CCU4_SLICE_IRQ_ID_EVENT0, XMC_CCU4_SLICE_SR_ID_0);
	//NVIC_EnableIRQ(CCU41_0_IRQn);

	XMC_CCU4_EnableShadowTransfer(CCU41, XMC_CCU4_SHADOW_TRANSFER_SLICE_3);
	//XMC_CCU4_EnableShadowTransfer(CCU41, XMC_CCU4_SHADOW_TRANSFER_PRESCALER_SLICE_3);

	XMC_POSIF_Start(POSIF1);
	XMC_CCU4_EnableClock(CCU41, 0);
	XMC_CCU4_EnableClock(CCU41, 2);
	XMC_CCU4_EnableClock(CCU41, 3);
	XMC_CCU4_SLICE_StartTimer(CCU41_CC40);
	XMC_CCU4_SLICE_StartTimer(CCU41_CC42);
	XMC_CCU4_SLICE_StartTimer(CCU41_CC43);
}

uint16_t getRawCount(){
	return XMC_CCU4_SLICE_GetTimerValue(CCU41_CC40);
}

int32_t getDirection(){
	if(XMC_POSIF_QD_GetDirection(POSIF1)){
		return 1;
	}else{
		return -1;
	}
}

int32_t getRawPeriod(){
	return XMC_CCU4_SLICE_GetCaptureRegisterValue(CCU41_CC43, 1) & 0xFFFF; //next 4 bits could be used for more precision (& 0xFFFFF) prescaler
}

double getWheelPosition(){
	return getRawCount() / TICKS_PER_WHEEL_REVOLUTION;
}

int32_t getWheelRevolutions(){
	return revolutions;
}

#define SYS_CLOCK (144UL * 1000UL * 1000UL) //hz
#define PRESCALER 128
#define CAPTURE_LENGTH 128
double getRawWheelSpeed(){
	double conversion = ((uint64_t)SYS_CLOCK * CAPTURE_LENGTH) / (PRESCALER * TICKS_PER_WHEEL_REVOLUTION);
	double res = conversion / getRawPeriod();

	if(isinf(res)){
		res = 0.0;
	}

	return res;
}

static double speedFiltered = 0.0;
double getWheelSpeed(){
	return speedFiltered;
}

bool startLog = false;
double buffer[BUFFER_SIZE];
double buffer2[BUFFER_SIZE];

#define ALPHA 0.85
void updateSpeedFilter(){
	static double oldValue = 0.0;
	static uint32_t sameValueCount = 0;
	double currentSpeed = getRawWheelSpeed();
	if(currentSpeed == oldValue){
		if(sameValueCount >= 50){
			currentSpeed = 0.0;
		}else{
			sameValueCount++;
		}
	} else {
		oldValue = currentSpeed;
		sameValueCount = 0;
	}
	speedFiltered = speedFiltered * ALPHA + currentSpeed * (1 - ALPHA);

	static uint32_t bufferIndex = 0;
		if(bufferIndex < BUFFER_SIZE && startLog){
			buffer[bufferIndex] = getRawWheelSpeed();
			buffer2[bufferIndex] = speedFiltered;
			bufferIndex++;
		}
}
