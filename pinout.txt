Motortreiber 1
P1.8	ND2		shared
P3.7	NSF		shared
P1.9	M1DIR
P1.11	M1PWM
P1.10	M2DIR
P3.10	M2PWM

POSIF1
P3.10	POSIF1.IN0B
P3.9	POSIF1.IN1B


# unused
#Motortreiber 2
#P1.8	ND2		shared
#P3.7	NSF		shared
#Px.x	M1DIR
#P2.12	M1PWM	untested
#Px.x	M2DIR
#P3.8	M2PWM	untested

IMU
P0.13	SCL
P3.15	SDA
P1.0	IRQ temporaer

EtherCAT (nicht Arduino)
P0.7	led_err
P6.3	led_link_act_p0
P3.12	led_link_act_p1
P0.8	led_run
P3.3	mdc
P0.12	mdio
P1.15	p0_link
P5.4	p0_rx_clk
P5.6	p0_rx_dv
P2.6	p0_rx_err
P5.0	p0_rxd0
P5.1	p0_rxd1
P5.2	p0_rxd2
P5.7	p0_rxd3
P5.5	p0_tx_clk
P6.1	p0_tx_ena
P6.2	p0_txd0
P6.4	p0_txd1
P6.5	p0_txd2
P6.6	p0_txd3
P3.4	p1_link
P0.1	p1_rx_clk
P0.9	p1_rx_dv
P15.2	p1_rx_err
P0.11	p1_rxd0
P0.6	p1_rxd1
P0.5	p1_rxd2
P0.4	p1_rxd3
P0.10	p1_tx_clk
P3.0	p1_tx_ena
P3.1	p1_txd0
P3.2	p1_txd1
P0.2	p1_txd2
P0.3	p1_txd3
P6.0	phy_clk25
P0.0	phy_reset

Debug UART via USB (nicht Arduino)
P1.4	RXD
P1.5	TXD