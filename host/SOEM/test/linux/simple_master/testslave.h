/*
 * Example EtherCat Slave driver header file
 * (c) Morten Mey & Tobias Drewes 2016
 */

#include <stdio.h>
#include <stdlib.h>

#include "ethercat.h"
#include "simple_master.h"

#ifndef TESTSLAVE_H
#define TESTSLAVE_H

extern struct drv_type_info_t testslave_input_types; //make available to other files
extern struct drv_type_info_t testslave_output_types;

void testslave_cb_op(int slave_idx, struct drv_info_t *info);
void testslave_cb_new(int slave_idx, struct drv_info_t *info);

#endif // TESTSLAVE_H
