/*
 * Example EtherCat Slave driver
 * (c) Morten Mey & Tobias Drewes 2016
 */

#include "testslave.h"

/*
 * Defines the ethercat data types used as inputs by the slave,
 * must be identical to the information used to create the Slave Stack Code.
 * For linux with gcc packed structs are needed,
 * as padding is enabled by default and breaks the bitstream parsing.
 */
struct __attribute__((packed)) testslave_input {
	float wheelspeed;
	float wheelpos;
	int32_t wheelrotations;
	boolean fault;
};

/*
 * Defines the input data types (same as above) to enable input parsing.
 */
struct drv_type_info_t testslave_input_types = { .types = { ECT_REAL32,ECT_REAL32,ECT_INTEGER32, ECT_BOOLEAN }, .numTypes = 4 };

/*
 * Defines the ethercat data types used as outputs by the slave,
 * must be identical to the information used to create the Slave Stack Code.
 * For linux with gcc packed structs are needed,
 * as padding is enabled by default and breaks the bitstream parsing.
 */
struct __attribute__((packed)) testslave_output{
	float targetwheelspeed;
	boolean enable;
};

/*
 * Defines the output data types (same as above) to enable output generation.
 */
struct drv_type_info_t testslave_output_types = { .types = { ECT_REAL32, ECT_BOOLEAN}, .numTypes = 2 };


float speed = 0.0;
boolean enable = FALSE;


void testslave_cb_op(int slave_idx, struct drv_info_t *info)
{
	/* Easy access to the user friendly parsed/generated buffers */
	struct testslave_input* input = info->input_buf;
	struct testslave_output* output = info->output_buf;
	
	printf("set %f, is %f, pos %f, rot %d, fault %hhu\r",
		speed,
		input->wheelspeed,
		input->wheelpos,
		input->wheelrotations,
		input->fault);
	
	/*printf("set %f, is %f, pos %f, rot %d, fault %hhu\r",
		data,
		*((float*)ec_slave[slave_idx].inputs),
		*((float*)ec_slave[slave_idx].inputs+1),
		*((int32_t*)ec_slave[slave_idx].inputs+2),
		*(ec_slave[slave_idx].inputs+12));/**/
	
	/*uint8_t *ptr = (uint8_t*)&data;
	ec_slave[slave_idx].outputs[0] = ptr[0];
	ec_slave[slave_idx].outputs[1] = ptr[1];
	ec_slave[slave_idx].outputs[2] = ptr[2];
	ec_slave[slave_idx].outputs[3] = ptr[3];
	ec_slave[slave_idx].outputs[4] = 1;*/
	//printf("%x %hhx %hhx %hhx %hhx\r", *((uint32_t*)&data), ptr[0],ptr[1],ptr[2],ptr[3]);
	output->targetwheelspeed = speed;
	output->enable = enable;
}

void testslave_cb_new(int slave_idx, struct drv_info_t *info)
{
	printf("new: %d\n", slave_idx);
	info->input_buf = malloc(sizeof(struct testslave_input));
	info->output_buf = malloc(sizeof(struct testslave_output));
	((struct testslave_output*)info->output_buf)->targetwheelspeed = 0.0f;
	((struct testslave_output*)info->output_buf)->enable = 0;
}

/*
Example for creating a drv_info_t struct:

struct drv_info_t testslave = {
	.vendor_id = 0x8D2,
	.device_name = "SSC_Device",
	.callback_op = testslave_cb_op,
	.callback_newslave = testslave_cb_new,
	.input_parsing_enable = TRUE,
	.output_generate_enable = TRUE,
	.input_typeinfo = testslave_input_types,
	.output_typeinfo = testslave_output_types
	};
*/