/*
 * Simple EtherCat Master
 *
 * Usage : simple_master [ifname1]
 * ifname is NIC interface, f.e. eth0
 *
 * (c) Morten Mey & Tobias Drewes 2016
 */

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <signal.h>

#include "simple_master.h"
#include "testslave.h"

struct ecat_master_info_t master_info;

/*
 * Signal handler to catch SIGINT to stop the master gracefully.
 */
void sigHandler(int dummy)
{
	printf("Stopping EtherCAT Master\n");
	master_info.stop = TRUE;
}

/* 
 * Variables and a repeatedly called timer function
 * used for the testslave driver.
 * Provides a simple cli program to set the motor speed
 */
extern float speed;
extern boolean enable;
int counter = 0;
void timer(void *param)
{
	while(1)
	{
		counter++;
		if(counter%10 == 0)
		{
			if(speed > 0)
				speed = -10.0f;
			else
				speed = 10.0f;
		}
		osal_usleep(1000*1000);
	}
}

int main(int argc, char *argv[])
{
	// This is an example of how to specify a slave driver information structure
	struct drv_info_t testslave = {
		.vendor_id = 0x8D2,				// Vendor ID and Device Name are used to
		.device_name = "SSC_Device",	// find driver for connected slaves.
		.callback_op = testslave_cb_op,
		.callback_newslave = testslave_cb_new, // If unused, set to NULL
		.input_parsing_enable = TRUE,
		.output_generate_enable = TRUE,
		.input_typeinfo = testslave_input_types,
		.output_typeinfo = testslave_output_types };
	
	if(argc == 1)
	{
		printf("Usage: simple_master ifname1\nifname = eth0 for example\n");
		return 0;
	}
	signal(SIGINT, sigHandler);
	init_ecat_master_info(&master_info, argv[1], &testslave, 1);
	init_ecat_master(&master_info);
	
	// Timer for another example app
	//OSAL_THREAD_HANDLE timer_handle;
	//osal_thread_create(&timer_handle, 128000, &timer, NULL);
	float dir = 1.0f;
	while(!master_info.stop)
	{
		switch(getchar()){
			case '0': speed = dir * 0.0f; break;
			case '1': speed = dir * 0.5f; break;
			case '2': speed = dir * 1.0f; break;
			case '3': speed = dir * 1.5f; break;
			case '4': speed = dir * 2.0f; break;
			case '5': speed = dir * 2.5f; break;
			case '6': speed = dir * 3.0f; break;
			case '7': speed = dir * 3.5f; break;
			case '8': speed = dir * 16.0f; break;
			case '9': speed = dir * 18.0f; break;
			case '-': dir = -dir; speed = -speed;break;
			case 'q': master_info.stop = TRUE; break;
			case 'e': enable = !enable; break;
			default: break;
		}
		//osal_usleep(1000000);
	}
	osal_usleep(20000);		// Wait for other threads
	return 0;
}
