/*
 * Header file for Simple EtherCat Master
 *
 * (c) Morten Mey & Tobias Drewes 2016
 * based on simple_test.c (c) Arthur Ketels 2010 - 2011
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ethercat.h"

#define EC_TIMEOUTMON 500


#ifndef SIMPLE_MASTER_H
#define SIMPLE_MASTER_H


/*
 * Contains information about the message structs a ecat driver uses.
 * Used to automagically convert and map beween the ecat bitstream and a buffer.
 */
struct drv_type_info_t{
	ec_datatype types[255];
	size_t numTypes;
};

/*
 * Contains Information about a specific ethercat device driver.
 */
struct drv_info_t
{
	/*
	 * Buffers for input parsing and output generation.
	 * Must be allocated by the driver, if input parsing or output generation is
	 * enabled. (See testslave.c for an example)
	 */
	void *input_buf,
		*output_buf;

	/*
	 * Set to TRUE if the master should parse the inputs and generate outputs.
	 * Requires a valid input/output typeinfo
	 * and a allocatetd input/output buffer.
	 */
	boolean input_parsing_enable, output_generate_enable;

	/* The typeinfo for parsing inputs and generating outputs. */
	struct drv_type_info_t input_typeinfo, output_typeinfo;

	/* This callback is called once for every ethercat packet. */
	void (*callback_op)(int slave_idx, struct drv_info_t *info);

	/*
	 * This callback is called once when a slave for this driver is found.
	 * Optional, set to NULL if unused.
	 */
	void (*callback_newslave)(int slave_idx, struct drv_info_t *info);

	/* The vendor_id of the slaves for which this driver is responsible. */
	uint32 vendor_id;

	/* The device_name of the slaves for which this driver is responsible. */
	char device_name[255];
};

/*
 * Contains information needed during runtime of the ecat master threads.
 * Also startup/initialization fields are passed to the threads via this struct.
 */
struct ecat_master_info_t
{
	/*
	 * Set to TRUE to put all slaves in state EC_STATE_INIT 
	 * and stop the ecat master threads.
	 */
	boolean stop;
	
	/*
	 * Path to the ethernet adapter used for ethercat communication.
	 */
	char *ifname;
	
	/*
	 * Array of available slave drivers + size of the array
	 */
	struct drv_info_t *av_drvs;
	size_t av_drv_cnt;
	
	/* Set to TRUE when the master is in operational state */
	boolean inOP;
	
	/* Internal (SOEM specific) variables */
	char IOmap[4096];
	OSAL_THREAD_HANDLE errthread;
	OSAL_THREAD_HANDLE workthread;
	int expectedWKC;
	volatile int wkc;
	uint8 currentgroup;
	struct drv_info_t slv_drvs[EC_MAXSLAVE];
};

/*
 * Initializes a master_info_t struct with all fields necessary for bringing
 * up a ethercat master.
 */
void init_ecat_master_info(struct ecat_master_info_t *info, char *ifname,
	struct drv_info_t *available_drivers, size_t num_drivers);

/*
 * Starts the 2 required ethercat master threads.
 * Returns 0 on success and -1 on failure to start one of the two threads.
 */
int init_ecat_master(struct ecat_master_info_t *info);

/*
 * Prints ethercat bus type info to stdout.
 * Useful for debugging.
 */
void printTypeInfo(struct drv_type_info_t* info);

/*
 * Determines the size of a ec_datatype in bytes.
 */
size_t sizeOfType(ec_datatype type);

/*
 * Calculates the size required by the type descriped by info.
 */
size_t sizeOfTypes(struct drv_type_info_t* info);

#endif // SIMPLE_MASTER_H
