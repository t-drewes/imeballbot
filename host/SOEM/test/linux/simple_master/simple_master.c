/*
 * Simple EtherCat Master
 *
 * (c) Morten Mey & Tobias Drewes 2016
 * based on simple_test.c (c) Arthur Ketels 2010 - 2011
 */
 
#include "simple_master.h"

/*
 * Reads the byte from the specified offset in the ethercat bitstream data.
 */
uint8_t get_byte(uint8_t* data, unsigned int bitOffset)
{
	data += bitOffset / 8;
	bitOffset %= 8;
	if(bitOffset == 0)
	{
		return *data;
	}
	else
	{
		return (((*data) >> bitOffset)) | ((*(data + 1) << (8 - bitOffset)));
	}
}

/*
 * Writes the byte val to the specified offset in the ethercat bitstream data.
 */
void set_byte(uint8_t* data, unsigned int bitOffset, uint8_t val)
{
	uint8_t mask;
	data += bitOffset / 8;
	bitOffset %= 8;
	mask = (1 << bitOffset) - 1;
	if(bitOffset == 0)
	{
		*data = val;
	}
	else
	{
		*data = (*data & mask) | val << bitOffset;
		*(data + 1) = (*(data + 1) & ~mask) | val >> (8 - bitOffset);
	}
}

/*
 * Maps the input from ecat devices to a driver INSTANCE specific input buffer.
 * (The input buffer may be overlayed with a driver specific packed struct)
 */
void parseInput(uint16_t slv_idx, uint8_t *in_buf, struct drv_type_info_t *info)
{
	size_t i, j;
	unsigned int startbit;

	size_t bytesToRead = 0;

	startbit = ec_slave[slv_idx].Istartbit;

	for(i = 0; i < info->numTypes; ++i)
	{
		int inputByte = startbit / 8;
		int inputBit = startbit % 8;
		switch(info->types[i])
		{
		case ECT_BOOLEAN:
			*((uint8_t*)in_buf)
				= (ec_slave[slv_idx].inputs[inputByte] >> (inputBit)) & 1;
			++startbit;
			++in_buf;
			bytesToRead = 0;
			break;
		case ECT_INTEGER8:
		case ECT_UNSIGNED8:
			bytesToRead = 1;
			break;
		case ECT_INTEGER16:
		case ECT_UNSIGNED16:
			bytesToRead = 2;
			break;
		case ECT_INTEGER32:
		case ECT_UNSIGNED32:
		case ECT_REAL32:
			bytesToRead = 4;
			break;
		case ECT_INTEGER64:
		case ECT_UNSIGNED64:
		case ECT_REAL64:
			bytesToRead = 8;
			break;
		default:
			fprintf(stderr, "Type not suported!");
			return;
			break;
		}

		for(j = 0; j < bytesToRead; ++j)
		{
			*((uint8_t*)in_buf + j)
				= get_byte(ec_slave[slv_idx].inputs, startbit + j * 8);
		}

		switch(bytesToRead)
		{
		case 2:
			*((uint16_t*)in_buf) = etohs(*((uint16_t*)in_buf));
			break;
		case 4:
			*((uint32_t*)in_buf) = etohl(*((uint32_t*)in_buf));
			break;
		case 8:
			*((uint64_t*)in_buf) = etohll(*((uint64_t*)in_buf));
			break;
		default:
			break;
		}

		in_buf += bytesToRead;
		startbit += bytesToRead * 8;
	}
}

/*
 * Maps a driver INSTANCE specific output buffer to the ecat output stream.
 * (The output buffer may be overlayed with a driver specific packed struct)
 */
void generateOutput(uint16_t slv_idx, uint8_t *out_buf,
	struct drv_type_info_t *info)
{
	size_t i, j;
	int startbit;
	uint8_t buf[8];
	size_t bytesToWrite = 0;

	startbit = ec_slave[slv_idx].Ostartbit;

	for(i = 0; i < info->numTypes; ++i)
	{
		int outputByte = startbit / 8;
		int outputBit = startbit % 8;
		switch(info->types[i])
		{
		case ECT_BOOLEAN:
			if(*(uint8_t*)(out_buf))
			{
				ec_slave[slv_idx].outputs[outputByte] |= 1 << outputBit;
			}
			else
			{
				ec_slave[slv_idx].outputs[outputByte] &= ~(1 << outputBit);
			}
			++startbit;
			++out_buf;
			bytesToWrite = 0;
			break;
		case ECT_INTEGER8:
		case ECT_UNSIGNED8:
			buf[0] = *((uint8_t*)out_buf);
			bytesToWrite = 1;
			break;
		case ECT_INTEGER16:
		case ECT_UNSIGNED16:
			*((uint16_t*)buf) = htoes(*(uint16_t*)out_buf);
			bytesToWrite = 2;
			break;
		case ECT_INTEGER32:
		case ECT_UNSIGNED32:
		case ECT_REAL32:
			*((uint32_t*)buf) = htoel(*(uint32_t*)out_buf);
			bytesToWrite = 4;
			break;
		case ECT_INTEGER64:
		case ECT_UNSIGNED64:
		case ECT_REAL64:
			*((uint64_t*)buf) = htoell(*(uint64_t*)out_buf);
			bytesToWrite = 8;
			break;
		default:
			fprintf(stderr, "Tpye not suported!");
			return;
			break;
		}

		for(j = 0; j < bytesToWrite; ++j)
		{
			set_byte(ec_slave[slv_idx].outputs, startbit + 8 * j, buf[j]);
		}

		out_buf += bytesToWrite;
		startbit += bytesToWrite * 8;
	}
}

/*
 * Determines the name of one ethercat bus datatype.
 */
char* getTypeName(ec_datatype type)
{
	switch(type)
	{
	case ECT_BOOLEAN:
		return "ECT_BOOLEAN";
	case ECT_UNSIGNED8:
		return "ECT_UNSIGNED8";
	case ECT_INTEGER8:
		return "ECT_INTEGER8";
	case ECT_UNSIGNED16:
		return "ECT_UNSIGNED16";
	case ECT_INTEGER16:
		return "ECT_INTEGER16";
	case ECT_UNSIGNED32:
		return "ECT_UNSIGNED32";
	case ECT_INTEGER32:
		return "ECT_INTEGER32";
	case ECT_REAL32:
		return "ECT_REAL32";
	case ECT_UNSIGNED64:
		return "ECT_UNSIGNED64";
	case ECT_INTEGER64:
		return "ECT_INTEGER64";
	case ECT_REAL64:
		return "ECT_REAL64";
	default:
		return "NOT_IMPLEMENTED";
	}
}

void printTypeInfo(struct drv_type_info_t* info)
{
	char* c = "";
	printf("{");
	for(size_t i = 0; i < info->numTypes; ++i)
	{
		printf("%s%s", c, getTypeName(info->types[i]));
		c = ", ";
	}
	printf("}");
}

size_t sizeOfType(ec_datatype type)
{
	switch(type)
	{
	case ECT_BOOLEAN:
	case ECT_INTEGER8:
	case ECT_UNSIGNED8:
		return 1;
	case ECT_INTEGER16:
	case ECT_UNSIGNED16:
		return 2;
	case ECT_INTEGER32:
	case ECT_UNSIGNED32:
	case ECT_REAL32:
		return 4;
	case ECT_INTEGER64:
	case ECT_UNSIGNED64:
	case ECT_REAL64:
		return 8;
	default:
		return 0;
	}
}

size_t sizeOfTypes(struct drv_type_info_t* info)
{
	size_t size = 0;
	for(size_t i = 0; i < info->numTypes; ++i)
	{
		size += sizeOfType(info->types[i]);
	}
	return size;
}

/*
 * Needs to be called when exiting the ecatmaster function.
 * (Needs to be called to close the ecat stream after calling ec_config_init())
 */
void cleanup_config_init()
{
	ec_close();
}

/*
 * Needs to be called when exiting the ecatmaster function.
 * (Needs to be called to reset all slaves back to init state after setting
 * slaves to operational state)
 */
void cleanup_init_slaves()
{
	ec_slave[0].state = EC_STATE_INIT;
	/* request INIT state for all slaves */
	ec_writestate(0);
}

/*
 * The ethercat master main thread (runtime loop).
 */
OSAL_THREAD_FUNC ecatmaster(void *ptr)
{
	struct ecat_master_info_t *mstinfo = ptr;
	
	if(!ec_init(mstinfo->ifname))
	{
		fprintf(stderr,"No socket connection on %s\nExcecute as root\n",
			mstinfo->ifname);
		return;
	}
	
	if(ec_config_init(FALSE) == 0)
	{
		fprintf(stderr, "No slaves found!\n");
		
		// clean up before ending the thread
		// ec_close() happens in cleanup_config_init()
		cleanup_config_init();
		return;
	}
	
	ec_config_map(&(mstinfo->IOmap)); // BUG?! should not be a char**
	ec_configdc();
	
	/* wait for all slaves to reach SAFE_OP state */
	ec_statecheck(0, EC_STATE_SAFE_OP,  EC_TIMEOUTSTATE * 4);
	
	mstinfo->expectedWKC = (ec_group[0].outputsWKC * 2) + ec_group[0].inputsWKC;
	
	ec_slave[0].state = EC_STATE_OPERATIONAL;
	/* send one valid process data to make outputs in slaves happy */
	ec_send_processdata();
	ec_receive_processdata(EC_TIMEOUTRET);
	/* request OP state for all slaves */
	ec_writestate(0);
	
	int chk = 40;
	/* wait for all slaves to reach OP state */
	do
	{
		ec_send_processdata();
		ec_receive_processdata(EC_TIMEOUTRET);
		ec_statecheck(0, EC_STATE_OPERATIONAL, 50000);
	}
	while(chk-- && (ec_slave[0].state != EC_STATE_OPERATIONAL));
	
	if(ec_slave[0].state != EC_STATE_OPERATIONAL)
	{
		fprintf(stderr, "Not all slaves reached operational state.\n");
		ec_readstate();
		for(int i = 1; i<=ec_slavecount ; i++)
		{
			if(ec_slave[i].state != EC_STATE_OPERATIONAL)
			{
				fprintf(stderr,
					"Slave %d State=0x%2.2x StatusCode=0x%4.4x : %s\n",
					i, ec_slave[i].state, ec_slave[i].ALstatuscode,
					ec_ALstatuscode2string(ec_slave[i].ALstatuscode));
			}
		}
		
		cleanup_init_slaves();
		cleanup_config_init();
		return;
	}
	
	/* Find and initialize slave drivers */
	for(uint16_t i = 1; i <= ec_slavecount; ++i)
	{
		mstinfo->slv_drvs[i].callback_op = NULL;
		for(size_t j = 0; j < mstinfo->av_drv_cnt; ++j)
		{
			if(mstinfo->av_drvs[j].vendor_id == ec_slave[i].eep_man
				&& !strcmp(mstinfo->av_drvs[j].device_name, ec_slave[i].name))
				{
					mstinfo->slv_drvs[i] = mstinfo->av_drvs[j];
					if(mstinfo->slv_drvs[i].callback_newslave)
					{
						mstinfo->slv_drvs[i].callback_newslave(i,
							&(mstinfo->slv_drvs[i]));
					}
				}
		}
	}
	
	/* ECAT Master is now online, run until stop is set */
	mstinfo->inOP = TRUE;
	while(!mstinfo->stop)
	{
		/* Small delay to avoid 100% cpu core load */
		osal_usleep(5000);
		
		ec_send_processdata();
		mstinfo->wkc = ec_receive_processdata(EC_TIMEOUTRET);
		
		if(mstinfo->wkc < mstinfo->expectedWKC)
		{
			continue;
		}
		for(uint16_t i = 1 ; i <= ec_slavecount; i++)
		{
			struct drv_info_t *drv = &(mstinfo->slv_drvs[i]);
			if(!drv->callback_op)
				continue;
			if(drv->input_parsing_enable)
			{
				parseInput(i, drv->input_buf, &(drv->input_typeinfo));
			}
			drv->callback_op(i, drv);
			if(drv->output_generate_enable)
			{
				generateOutput(i, drv->output_buf, &(drv->output_typeinfo));
			}
		}
	}
	mstinfo->inOP = FALSE;
	
	cleanup_init_slaves();
	cleanup_config_init();
}

/* 
 * The ethercat master slave error handling thread.
 */
OSAL_THREAD_FUNC ecatcheck(void *ptr)
{
	struct ecat_master_info_t *mstinfo = ptr;
	
	int slave;
	while(!mstinfo->stop)
	{
		if(mstinfo->inOP && ((mstinfo->wkc < mstinfo->expectedWKC) || ec_group[mstinfo->currentgroup].docheckstate))
		{
			/* one ore more slaves are not responding */
			ec_group[mstinfo->currentgroup].docheckstate = FALSE;
			ec_readstate();
			for(slave = 1; slave <= ec_slavecount; slave++)
			{
				if((ec_slave[slave].group == mstinfo->currentgroup)
					&& (ec_slave[slave].state != EC_STATE_OPERATIONAL))
				{
					ec_group[mstinfo->currentgroup].docheckstate = TRUE;
					if(ec_slave[slave].state
						== (EC_STATE_SAFE_OP + EC_STATE_ERROR))
					{
						fprintf(stderr, "ERROR : slave %d is in SAFE_OP + ERROR, attempting ack.\n", slave);
						ec_slave[slave].state = (EC_STATE_SAFE_OP + EC_STATE_ACK);
						ec_writestate(slave);
					}
					else if(ec_slave[slave].state == EC_STATE_SAFE_OP)
					{
						fprintf(stderr, "WARNING : slave %d is in SAFE_OP, change to OPERATIONAL.\n", slave);
						ec_slave[slave].state = EC_STATE_OPERATIONAL;
						ec_writestate(slave);
					}
					else if(ec_slave[slave].state > 0)
					{
						if(ec_reconfig_slave(slave, EC_TIMEOUTMON))
						{
							ec_slave[slave].islost = FALSE;
							fprintf(stderr, "MESSAGE : slave %d reconfigured\n",slave);
						}
					}
					else if(!ec_slave[slave].islost)
					{
						/* re-check state */
						ec_statecheck(slave, EC_STATE_OPERATIONAL, EC_TIMEOUTRET);
						if(!ec_slave[slave].state)
						{
							ec_slave[slave].islost = TRUE;
							fprintf(stderr, "ERROR : slave %d lost\n",slave);
						}
					}
				}
				if(ec_slave[slave].islost)
				{
					if(!ec_slave[slave].state)
					{
						if(ec_recover_slave(slave, EC_TIMEOUTMON))
						{
							ec_slave[slave].islost = FALSE;
							fprintf(stderr, "MESSAGE : slave %d recovered\n",slave);
						}
					}
					else
					{
						ec_slave[slave].islost = FALSE;
						fprintf(stderr, "MESSAGE : slave %d found\n",slave);
					}
				}
			}
			if(!ec_group[mstinfo->currentgroup].docheckstate)
				fprintf(stderr, "OK : all slaves resumed OPERATIONAL.\n");
		}
		osal_usleep(10000);
	}
}

void init_ecat_master_info(struct ecat_master_info_t *info, char *ifname,
	struct drv_info_t *available_drivers, size_t num_drivers)
{
	info->currentgroup = 0;
	info->inOP = FALSE;
	info->stop = FALSE;
	info->ifname = ifname;
	info->av_drvs = available_drivers;
	info->av_drv_cnt = num_drivers;
}

int init_ecat_master(struct ecat_master_info_t *info)
{
	//create thread to handle slave error handling in OP */
	int status = osal_thread_create(&(info->errthread), 128000, &ecatcheck, info);
	if(!status)
		return -1;
	// start cyclic part
	status = osal_thread_create(&(info->workthread), 128000, &ecatmaster, info);
	if(!status)
		return -1;
	else
		return 0;
}
