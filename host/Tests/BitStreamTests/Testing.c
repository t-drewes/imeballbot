#include "Testing.h"
#include <string.h>

static uint8_t data[8] = { 0x1, 0x81, 0x41, 0x21, 0x11, 0x9, 0x5, 0x3 };

uint8_t runReadByteTest(){
	uint8_t buf[2];
	buf[0] = 1;
	buf[1] = 1;

	uint8_t error = 0;
	
	for (unsigned int i = 0; i < 8; ++i){
		uint8_t res = get_byte(buf, i);
		if (res != data[i]){
			error = 1;
		}
		buf[0] = buf[0] << 1;
	}

	return error;
}
uint8_t runWriteByteTest(){
	uint8_t buf[2];
	uint8_t error = 0;

	for (unsigned int i = 0; i < 8; ++i){
		set_byte(buf, i, data[i]);
		if (buf[1] != 1 && buf[0] >> i != 1){
			error = 1;
		}
	}
	return error;
}

void generateOne(ec_datatype type, uint8_t* buf){
	switch (type){
	case ECT_BOOLEAN:
		*buf = (uint8_t) rand() % 2;
		return;
	case ECT_INTEGER8:
	case ECT_UNSIGNED8:
		*buf = (uint8_t) rand();
		return;
	case ECT_INTEGER16:
	case ECT_UNSIGNED16:
		*((uint16_t*)buf) = (uint16_t)rand();
		return;
	case ECT_INTEGER32:
	case ECT_UNSIGNED32:
	case ECT_REAL32:
		*((uint32_t*)buf) = (uint32_t)rand();
		return;
	case ECT_INTEGER64:
	case ECT_UNSIGNED64:
	case ECT_REAL64:
		*((uint64_t*)buf) = (uint64_t)rand();
		return;
	}
}

void generateAll(drv_type_info_t* info, uint8_t* buf) {
	size_t offset = 0;
	for (size_t i = 0; i < info->numTypes; ++i){
		generateOne(info->types[i], buf + offset);
		offset += sizeOfType(info->types[i]);
	}
}

uint8_t checkOne(ec_datatype type, uint8_t* in, uint8_t* out){
	return memcmp(in, out, sizeOfType(type)) != 0;
}

void printType(ec_datatype type, uint8_t* in, uint8_t* out){
	size_t size = sizeOfType(type);
	if (size == 8){
		uint64_t a, b;
		memcpy(&a, in, size);
		memcpy(&b, out, size);
		printf("%d != %d", a, b);
	}
	else if (size == 4){
		uint32_t a, b;
		memcpy(&a, in, size);
		memcpy(&b, out, size);
		printf("%d != %d", a, b);
	}
	else if (size == 2){
		uint16_t a, b;
		memcpy(&a, in, size);
		memcpy(&b, out, size);
		printf("%d != %d", a, b);
	}
	else if (size == 1){
		uint8_t a, b;
		memcpy(&a, in, size);
		memcpy(&b, out, size);
		printf("%d != %d", a, b);
	}
}

uint8_t checkAll(drv_type_info_t* info, uint8_t* in, uint8_t* out) {
	size_t offset = 0;
	uint8_t error = 0;
	for (size_t i = 0; i < info->numTypes; ++i){
		if (checkOne(info->types[i], in + offset, out + offset)){
			printf("Mismatch in type No: %d ", i);
			printType(info->types[i], in + offset, out + offset);
			printf("\n");
			error = 1;
		}
		offset += sizeOfType(info->types[i]);
	}
	return error;
}

uint8_t maskByte(uint8_t byte, unsigned int bitoffset, uint8_t upper) {
	uint8_t mask = (1 << bitoffset) - 1;
	if (upper) {
		mask = ~mask;
	}
	return byte & mask;
}

uint8_t runTest(drv_type_info_t* info, uint32_t runs) {
	size_t size = sizeOfTypes(info);
	uint8_t* in = malloc(size);
	uint8_t* out = malloc(size);
	uint8_t startbit;
	uint8_t startByteRandomData, endByteRandomData;

	for (uint32_t i = 0; i < runs; ++i){
		startbit = rand() % 8;
		ec_slave[0].Istartbit = startbit;
		ec_slave[0].Ostartbit = startbit;
		if (startbit != 0) {
			startByteRandomData = rand();
			endByteRandomData = rand();
			ec_slave[0].outputs[0] = startByteRandomData;
			ec_slave[0].outputs[size] = endByteRandomData;
		}
		generateAll(info, out);
		generateOutput(0, out, info);
		if (startbit != 0) {
			uint8_t preserved = maskByte(startByteRandomData,startbit, 0) == maskByte(ec_slave[0].outputs[0], startbit, 0);
			preserved = preserved && maskByte(endByteRandomData, startbit, 1) == maskByte(ec_slave[0].outputs[size], startbit, 1);
			if (!preserved) {
				printf("Bits not preserved when generating output! Iter %d\n", i);
			}
		}
		memcpy(ec_slave[0].inputs, ec_slave[0].outputs, 1024);
		parseInput(0, in, info);
		if (checkAll(info, in, out)){
			printf(" Iter %d\n", i);
			free(in);
			free(out);
			return 1;
		}
	}

	free(in);
	free(out);

	return 0;
}