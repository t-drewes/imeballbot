#pragma once
#include<stdint.h>
#include<stdio.h>
#include<stdarg.h>

typedef enum
{
	ECT_BOOLEAN = 0x0001,
	ECT_INTEGER8 = 0x0002,
	ECT_INTEGER16 = 0x0003,
	ECT_INTEGER32 = 0x0004,
	ECT_UNSIGNED8 = 0x0005,
	ECT_UNSIGNED16 = 0x0006,
	ECT_UNSIGNED32 = 0x0007,
	ECT_REAL32 = 0x0008,
	ECT_VISIBLE_STRING = 0x0009,
	ECT_OCTET_STRING = 0x000A,
	ECT_UNICODE_STRING = 0x000B,
	ECT_TIME_OF_DAY = 0x000C,
	ECT_TIME_DIFFERENCE = 0x000D,
	ECT_DOMAIN = 0x000F,
	ECT_INTEGER24 = 0x0010,
	ECT_REAL64 = 0x0011,
	ECT_INTEGER64 = 0x0015,
	ECT_UNSIGNED24 = 0x0016,
	ECT_UNSIGNED64 = 0x001B,
	ECT_BIT1 = 0x0030,
	ECT_BIT2 = 0x0031,
	ECT_BIT3 = 0x0032,
	ECT_BIT4 = 0x0033,
	ECT_BIT5 = 0x0034,
	ECT_BIT6 = 0x0035,
	ECT_BIT7 = 0x0036,
	ECT_BIT8 = 0x0037
} ec_datatype;

#define etohs(A) (A)
#define etohl(A) (A)
#define etohll(A) (A)
#define htoes(A) (A)
#define htoel(A) (A)
#define htoell(A) (A)

typedef struct{
	ec_datatype types[255];
	size_t numTypes;
} drv_type_info_t;

typedef struct{
	unsigned int Istartbit;
	unsigned int Ostartbit;
	uint8_t outputs[1024];
	uint8_t inputs[1024];
} Slaves;

Slaves ec_slave[200];

uint8_t get_byte(uint8_t* data, unsigned int bitOffset);
void set_byte(uint8_t* data, unsigned int bitOffset, uint8_t val);

void parseInput(uint16_t slave, uint8_t* data, drv_type_info_t* info);
void generateOutput(uint16_t slave, uint8_t* data, drv_type_info_t* info);

void printTypeInfo(drv_type_info_t* info);

size_t sizeOfType(ec_datatype type);

size_t sizeOfTypes(drv_type_info_t* info);