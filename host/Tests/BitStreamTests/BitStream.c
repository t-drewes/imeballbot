#include "BitStream.h"

uint8_t get_byte(uint8_t* data, unsigned int bitOffset) {
	data += bitOffset / 8;
	bitOffset %= 8;
	if (bitOffset == 0){
		return *data;
	}
	else{
		return (((*data) >> bitOffset)) | ((*(data + 1) << (8 - bitOffset)));
	}
}

void set_byte(uint8_t* data, unsigned int bitOffset, uint8_t val){
	uint8_t mask;
	data += bitOffset / 8;
	bitOffset %= 8;
	mask = (1 << bitOffset) - 1;
	if (bitOffset == 0){
		*data = val;
	} else{
		*data = (*data & mask) | val << bitOffset;
		*(data + 1) = (*(data + 1) & ~mask) | val >> (8 - bitOffset);
	}
}

void parseInput(uint16_t slave, uint8_t* data, drv_type_info_t* info){
	size_t i, j;
	unsigned int startbit;

	size_t bytesToRead = 0;

	startbit = ec_slave[slave].Istartbit;

	for (i = 0; i < info->numTypes; ++i){
		int inputByte = startbit / 8;
		int inputBit = startbit % 8;
		switch (info->types[i]){
		case ECT_BOOLEAN:
			*((uint8_t*)data) = (ec_slave[slave].inputs[inputByte] >> (inputBit)) & 1;
			++startbit;
			++data;
			bytesToRead = 0;
			break;
		case ECT_INTEGER8:
		case ECT_UNSIGNED8:
			bytesToRead = 1;
			break;
		case ECT_INTEGER16:
		case ECT_UNSIGNED16:
			bytesToRead = 2;
			break;
		case ECT_INTEGER32:
		case ECT_UNSIGNED32:
		case ECT_REAL32:
			bytesToRead = 4;
			break;
		case ECT_INTEGER64:
		case ECT_UNSIGNED64:
		case ECT_REAL64:
			bytesToRead = 8;
			break;
		default:
			fprintf(stderr, "Tpye not suported!");
			return;
			break;
		}

		for (j = 0; j < bytesToRead; ++j){
			*((uint8_t*)data + j) = get_byte(ec_slave[slave].inputs, startbit + j * 8);
		}

		switch (bytesToRead)
		{
		case 2:
			*((uint16_t*)data) = etohs(*((uint16_t*)data));
			break;
		case 4:
			*((uint32_t*)data) = etohl(*((uint32_t*)data));
			break;
		case 8:
			*((uint64_t*)data) = etohll(*((uint64_t*)data));
			break;
		default:
			break;
		}

		data += bytesToRead;
		startbit += bytesToRead * 8;
	}
}

void generateOutput(uint16_t slave, uint8_t* data, drv_type_info_t* info){
	size_t i, j;
	int startbit;
	uint8_t buf[8];
	size_t bytesToWrite = 0;

	startbit = ec_slave[slave].Ostartbit;

	for (i = 0; i < info->numTypes; ++i){
		int outputByte = startbit / 8;
		int outputBit = startbit % 8;
		switch (info->types[i]) {
		case ECT_BOOLEAN:

			if (*(uint8_t*)(data)) {
				ec_slave[slave].outputs[outputByte] |= 1 << outputBit;
			} else {
				ec_slave[slave].outputs[outputByte] &= ~(1 << outputBit);
			}
			++startbit;
			++data;
			bytesToWrite = 0;
			break;
		case ECT_INTEGER8:
		case ECT_UNSIGNED8:
			buf[0] = *((uint8_t*)data);
			bytesToWrite = 1;
			break;
		case ECT_INTEGER16:
		case ECT_UNSIGNED16:
			*((uint16_t*)buf) = htoes(*(uint16_t*)data);
			bytesToWrite = 2;
			break;
		case ECT_INTEGER32:
		case ECT_UNSIGNED32:
		case ECT_REAL32:
			*((uint32_t*)buf) = htoel(*(uint32_t*)data);
			bytesToWrite = 4;
			break;
		case ECT_INTEGER64:
		case ECT_UNSIGNED64:
		case ECT_REAL64:
			*((uint64_t*)buf) = htoell(*(uint64_t*)data);
			bytesToWrite = 8;
			break;
		default:
			fprintf(stderr, "Tpye not suported!");
			return;
			break;
		}

		for (j = 0; j < bytesToWrite; ++j){
			set_byte(ec_slave[slave].outputs, startbit + 8*j, buf[j]);
		}

		data += bytesToWrite;
		startbit += bytesToWrite * 8;
	}
}

char* getTypeName(ec_datatype type) {
	switch (type) {
	case ECT_BOOLEAN:
		return "ECT_BOOLEAN";
	case ECT_UNSIGNED8:
		return "ECT_UNSIGNED8";
	case ECT_INTEGER8:
		return "ECT_INTEGER8";
	case ECT_UNSIGNED16:
		return "ECT_UNSIGNED16";
	case ECT_INTEGER16:
		return "ECT_INTEGER16";
	case ECT_UNSIGNED32:
		return "ECT_UNSIGNED32";
	case ECT_INTEGER32:
		return "ECT_INTEGER32";
	case ECT_REAL32:
		return "ECT_REAL32";
	case ECT_UNSIGNED64:
		return "ECT_UNSIGNED64";
	case ECT_INTEGER64:
		return "ECT_INTEGER64";
	case ECT_REAL64:
		return "ECT_REAL64";
	default:
		return "NOT_IMPLEMENTED";
	}
}

void printTypeInfo(drv_type_info_t* info) {
	char* c = "";
	printf("{");
	for (size_t i = 0; i < info->numTypes; ++i) {
		printf("%s%s", c, getTypeName(info->types[i]));
		c = ", ";
	}
	printf("}");
}

size_t sizeOfType(ec_datatype type) {
	switch (type) {
	case ECT_BOOLEAN:
	case ECT_INTEGER8:
	case ECT_UNSIGNED8:
		return 1;
	case ECT_INTEGER16:
	case ECT_UNSIGNED16:
		return 2;
	case ECT_INTEGER32:
	case ECT_UNSIGNED32:
	case ECT_REAL32:
		return 4;
	case ECT_INTEGER64:
	case ECT_UNSIGNED64:
	case ECT_REAL64:
		return 8;
	default:
		return 0;
	}
}

size_t sizeOfTypes(drv_type_info_t* info) {
	size_t size = 0;
	for (size_t i = 0; i < info->numTypes; ++i) {
		size += sizeOfType(info->types[i]);
	}
	return size;
}