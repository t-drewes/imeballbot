#include "Testing.h"
#include <time.h>

#define NUM_RUNS 10000

#define NUM_CASES 7
#define MIN_NUM_RANDOM_TYPES 1
#define MAX_NUM_RANDOM_TYPES 100
#define NUM_RANDOM_TESTS 1000

drv_type_info_t testCases[NUM_CASES];

void initCases(){
	testCases[0] = (drv_type_info_t) { .types = { ECT_BOOLEAN }, .numTypes = 1 };
	testCases[1] = (drv_type_info_t) { .types = { ECT_UNSIGNED8 }, .numTypes = 1 };
	testCases[2] = (drv_type_info_t) { .types = { ECT_UNSIGNED16 }, .numTypes = 1 };
	testCases[3] = (drv_type_info_t) { .types = { ECT_UNSIGNED32 }, .numTypes = 1 };
	testCases[4] = (drv_type_info_t) { .types = { ECT_UNSIGNED64 }, .numTypes = 1 };
	testCases[5] = (drv_type_info_t) { .types = { ECT_REAL32, ECT_BOOLEAN, ECT_INTEGER64 }, .numTypes = 3 };
	testCases[6] = (drv_type_info_t) { .types = { ECT_INTEGER16, ECT_BOOLEAN, ECT_REAL64, ECT_BOOLEAN, ECT_UNSIGNED32 }, .numTypes = 5 };
}

ec_datatype generateRandomType() {
	switch (rand() % 11) {
	case 0:
		return ECT_BOOLEAN;
	case 1:
		return ECT_UNSIGNED8;
	case 2:
		return ECT_INTEGER8;
	case 3:
		return ECT_UNSIGNED16;
	case 4:
		return ECT_INTEGER16;
	case 5:
		return ECT_UNSIGNED32;
	case 6:
		return ECT_INTEGER32;
	case 7:
		return ECT_REAL32;
	case 8:
		return ECT_UNSIGNED64;
	case 9:
		return ECT_INTEGER64;
	case 10:
		return ECT_REAL64;
	default:
		return ECT_BOOLEAN; // should never happen, this is here to get rid of compiler warning
	}

}

void generateRandomTypeInfo(drv_type_info_t* info, size_t types) {
	info->numTypes = types;

	for (size_t i = 0; i < types; ++i) {
		info->types[i] = generateRandomType();
	}
}

int main(int argc, char** argv){
	char c;
	initCases();
	srand(time(NULL)); //init rng...

	printf("Starting tests...\n");
	runReadByteTest();
	runWriteByteTest();
	for (size_t i = 0; i < NUM_CASES; ++i){
		if (runTest(&testCases[i], NUM_RUNS)){
			fread(&c, 1, 1, stdin);
			return 1;
		}
		printf("Ran test No: %d...\n", i + 1);
	}

	printf("Starting random tests...\n");
	drv_type_info_t randomTypes = { .types = { ECT_BOOLEAN, ECT_REAL64 }, .numTypes = 2 };
	for (size_t test = 0; test < NUM_RANDOM_TESTS; ++test) { //this loop may take some time, definetly enable optimizations in compiler
		size_t numTypes = rand() % (MAX_NUM_RANDOM_TYPES - MIN_NUM_RANDOM_TYPES + 1);
		numTypes += MIN_NUM_RANDOM_TYPES;
		generateRandomTypeInfo(&randomTypes, numTypes);
		if (runTest(&randomTypes, NUM_RUNS)) {
			printTypeInfo(&randomTypes);
			fread(&c, 1, 1, stdin);
			return 1;
		}
		printf("Ran random test No: %d with %d types...\n", test, numTypes);
	}

	printf("All tests successful!");
	fread(&c, 1, 1, stdin);
	return 0;
}