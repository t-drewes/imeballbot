/*
 * MotorDriver.h
 *
 *  Created on: May 12, 2016
 *      Author: Morten Mey
 */

#ifndef MOTORDRIVER_H_
#define MOTORDRIVER_H_

#include<stdint.h>

/**
 * Call Dave_Init() before using the MotorDriver.
 *	setEnable(1) = turn On
 *	setEnable(0) = turn Off
 */
void setEnabled(uint8_t power);
/*
 * 0 if the motor is off,
 * something else otherwise.
 */
uint8_t getEnabled();

/**
 * Set the speed of the motor.
 * The value should be between -1 and 1.
 * If it is outside this range, it will be limited to this range.
 * Which direction is positive and which negative depends, on how
 * the motor is wired to the motor driver board.
 */
void setM1Speed(double speed);

/**
 * Not currently implemented.
 * Get the current current of the motor in milli amps,
 * as reported by the motor driver board.
 */
uint32_t getM1CurrentMilliamps();

/**
 * Returns a 0 if everything is ok,
 * something else in case of a fault.
 * If the motor is not enabled fault will be true.
 */
uint8_t getFault();

#endif /* MOTORDRIVER_H_ */
