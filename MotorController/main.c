/*
 * main.c
 *
 *  Created on: 2016 May 12 08:50:12
 *  Author: Morten Mey
 */

// required for ethercat structs to work correctly
#define OBJ_STRUCT_PACKED_START __attribute__ ((packed))


#include <DAVE.h>                 //Declarations from DAVE Code Generation (includes SFR declaration)
#include "PrintfRedirect.h"
#include <stdio.h>
#include "MotorDriver.h"
#include "HardwareEncoder.h"
#include "MotorController.h"
#include "SSC/Src/XMC_ESCObjects.h"

uint32_t counter = 0;
uint8_t fault = 0;

//setup pid controller values determined by experimentation, except Ta
//TODO get better values
struct MotorController_t controller = {
		.Ta = 0.001, //1ms
		.Kp = 15,
		.Ki = 20,
		.Kd = 0.05
};

uint8_t ethercatActive = 0;

//called when ethercat goes from SAFEOP to OP
void startOuput(){
	ethercatActive = 1;
}

//called when ethercat goes from OP to SAFEOP
void stopOutput(){
	ethercatActive = 0;
}

//called from ethercat MainLoop()
void ethercatCallback(TOBJ7000 *OUT_GENERIC, TOBJ6000 *IN_GENERIC)
{
	//stupid pointer cast, because the ssc tool turns floats into uint32
	float targetSpeed = *((float*)&(OUT_GENERIC->TargetWheelSpeed));
	uint8_t enable = OUT_GENERIC->Enable;

	if(ethercatActive){
		setEnabled(enable);
		setTargetSpeed(&controller, targetSpeed);
	}else{
		//safeop motor off, controller set to 0
		setEnabled(0);
		setTargetSpeed(&controller, 0.0);
	}

	float speed = (float) getWheelSpeed();
	float position = (float) getWheelPosition();
	int32_t rotations = getWheelRevolutions();
	speed*= getDirection();

	//again stupid pointer cast see above
	*((float*) &(IN_GENERIC->WheelSpeed)) = speed;
	*((float*) &(IN_GENERIC->WheelPosition)) = position;
	IN_GENERIC->WheelRotations = rotations;
	IN_GENERIC->Fault = fault;
}

/*
 * Called ever 1ms by systimer.
 */
void controllerCallback(void* data){
	updateSpeedFilter();
	updateControl(&controller);
	fault = getFault();
}

/**

 * @brief main() - Application entry point
 *
 * <b>Details of function</b><br>
 * This routine is the application entry point. It is invoked by the device startup code. It is responsible for
 * invoking the APP initialization dispatcher routine - DAVE_Init() and hosting the place-holder for user application
 * code.
 */

int main(void)
{
  DAVE_STATUS_t status;

  status = DAVE_Init();           /* Initialization of DAVE APPs  */

  initEncoder();

  initController(&controller);

  if(status == DAVE_STATUS_FAILURE)
  {
    /* Placeholder for error handler code. The while loop below can be replaced with an user error handler. */
    XMC_DEBUG("DAVE APPs initialization failed\n");

    while(1U)
    {

    }
  }

  //create timer for controllerCallback, note: value in microseconds
  uint32_t timer = SYSTIMER_CreateTimer(1000, SYSTIMER_MODE_PERIODIC, controllerCallback, NULL);
  SYSTIMER_StartTimer(timer);

  myPrintf("STARTING\n");
  while(1U)
  {
	  MainLoop();
  }
}
