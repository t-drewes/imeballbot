/*
 * HardwareEncoder.h
 *
 *  Created on: Jul 7, 2016
 *      Author: Morten Mey
 */

#ifndef HARDWAREENCODER_H_
#define HARDWAREENCODER_H_

#include <XMCLib/inc/xmc_posif.h>
#include <XMCLib/inc/xmc_ccu4.h>

/*
 * This function initializes POSIF1 and CCU41 for the Encoder.
 */
void initEncoder();

/*
 * Returns the raw tick count, representing the current wheel Position.
 */
uint16_t getRawCount();

/*
 * Returns 1 or -1 to indicate in which direction the motor is turning.
 * Which direction is considered positive depends on how the wires from the posif
 * to the encoder are connected.
 */
int32_t getDirection();

/*
 * Returns the raw tick count, representing the time between certain events.
 * This value is used to calculate the speed.
 */
int32_t getRawPeriod();

/*
 * Returns the current wheel position as a double between 0 and 1, where 0 is
 * the position the wheel had, when encoderInit() was called.
 */
double getWheelPosition();

/*
 * Returns the number of wheel revolutions since the encoder was initialized.
 * This number decreases if the wheel is turning in the negative direction.
 * Thus a value of e.g. 150 can mean, that the wheel turned 150 times in
 * the positive direction, or that the wheel turned 300 times in the positive
 * direction and 150 times in the negative direction, or ...
 */
int32_t getWheelRevolutions();

/*
 * Returns the unfiltered speed of the wheel in wheel rotations per second.
 * This function will almost never return 0, even if the wheel is standing still.
 * This is because, we get no impulses from the encoder when the wheel is not moving,
 * thus the timer gets no impulses and does not change.
 */
double getRawWheelSpeed();

/*
 * Returns the filtered speed of the wheel in wheel rotations per second.
 */
double getWheelSpeed();

/*
 * Updates the filter for the wheel speed. Should be called about once
 * every millisecond to keep the filtered speed value up to date.
 */
void updateSpeedFilter();

/*
 * A flag and several buffers to record data, which can then be send via
 * serial to a computer for further analysis.
 */
#define BUFFER_SIZE 5000
extern bool startLog;
extern double buffer[BUFFER_SIZE];
extern double buffer2[BUFFER_SIZE];

#endif /* HARDWAREENCODER_H_ */
