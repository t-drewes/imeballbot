/*
 * MotorController.c
 *
 *  Created on: Jul 14, 2016
 *      Author: Morten Mey
 */

#include "MotorController.h"

#include "MotorDriver.h"
#include "HardwareEncoder.h"

void updateControl(struct MotorController_t* cont){
	if(!getEnabled()){
		//reset summedError if motor is off
		cont->summedError = 0.0;
		return;
	}
	//calculate error divide by 30 so error is between -1 and 1.
	//Should be 25 since that is the maximum wheel speed. This however requires retuning of the controller.
	double e = (cont->targetSpeed - getWheelSpeed() * getDirection()) / 30;
	//standard pid controller
	cont->summedError+= e;
	double output = cont->Kp * e + cont->Ki * cont->Ta * cont->summedError + cont->Kd * (e - cont->lastError)/cont->Ta;
	cont->lastError = e;

	//anti wind up
	double limited = output;
	if(output > 1.0){
		limited = 1.0;
	}else if(output < -1.0){
		limited = -1.0;
	}
	cont->summedError += cont->Ta * (limited - output);

	cont->currentOutput = limited;
	setM1Speed(cont->currentOutput);
}

void setTargetSpeed(struct MotorController_t* cont, double speedInWheelRotationsPerSecond){
	cont->targetSpeed = speedInWheelRotationsPerSecond;
	cont->summedError = 0.0;
}

void initController(struct MotorController_t* cont){
	cont->summedError = 0.0;
	cont->lastError = 0.0;
	cont->currentOutput = 0.0;
	cont->targetSpeed = 0.0;
}
