/*
 * MotorContoller.h
 *
 *  Created on: Jul 14, 2016
 *      Author: Morten Mey
 */

#ifndef MOTORCONTROLLER_H_
#define MOTORCONTROLLER_H_

#define MAX_SUMMED_ERROR 0.0

struct MotorController_t{
	double targetSpeed; //in wheel rotations per second
	double lastError;
	double summedError;
	double currentOutput; //current value send to motor
	double Kp; //pid controller values
	double Ki;
	double Kd;
	double Ta; //interval time in seconds
};

/*
 * Call regularly to update the controller.
 * The time between calls should be equal to the Ta value in cont.
 */
void updateControl(struct MotorController_t* cont);

/*
 * Sets the target speed potentially reseting error values in the struct.
 */
void setTargetSpeed(struct MotorController_t* cont, double speedInWheelRotationsPerSecond);

/*
 * Initializes some values of cont. Kp, Ki, Kd, Ta are unchanged and must be set manually.
 */
void initController(struct MotorController_t* cont);

#endif /* MOTORCONTROLLER_H_ */
