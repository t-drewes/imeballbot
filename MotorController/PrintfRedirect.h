#include<DAVE.h>
#include<string.h>
#include<stdio.h>
#include<stdarg.h>

#ifndef PRINTFREDIRECT_H_
#define PRINTFREDIRECT_H_

/*
 * Writes buf to uart. Should theoretically be used by printf.... (printf still does not work)
 */
int _write(int file, char *buf, int nbytes)
{
	UART_STATUS_t result = UART_Transmit(&UART_0, (uint8_t*) buf, nbytes);
	/*for(int i = 0; i < nbytes; i++)
	{
		if(USIC_IsTxFIFOfull(UART001_Handle0.UartRegs)) // If the UART Tx fifo is full,
			return i; // Give up and report how many bytes we did send.
		else
			UART001_WriteData(UART001_Handle0,*buf++); // If the Tx fifo is not full, then go ahead and write the byte.
	}*/
	return result == UART_STATUS_SUCCESS ? nbytes : 0;
}

/*
 * A custom printf routine for printing via uart.
 * Same syntax as printf.
 */
int myPrintf(char* string, ...){
	static char buf[256];
	va_list va;
	va_start(va, string);
	vsnprintf(buf, 256, string, va);
	va_end(va);
	return _write(0, buf, strlen(buf));
}

#endif
