################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Dave/Generated/GLOBAL_CCU4/global_ccu4.c \
../Dave/Generated/GLOBAL_CCU4/global_ccu4_conf.c 

OBJS += \
./Dave/Generated/GLOBAL_CCU4/global_ccu4.o \
./Dave/Generated/GLOBAL_CCU4/global_ccu4_conf.o 

C_DEPS += \
./Dave/Generated/GLOBAL_CCU4/global_ccu4.d \
./Dave/Generated/GLOBAL_CCU4/global_ccu4_conf.d 


# Each subdirectory must supply rules for building sources it contributes
Dave/Generated/GLOBAL_CCU4/%.o: ../Dave/Generated/GLOBAL_CCU4/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM-GCC C Compiler'
	"C:\DAVEv4-64Bit\DAVE-4.2.4\eclipse\ARM-GCC-49/bin/arm-none-eabi-gcc" -MMD -MT "$@" -DXMC4800_F144x2048 -I"C:\Workspaces\DAVE-4.2-64Bit\WS_2015_03_02\EtherCatTest\Libraries\XMCLib\inc" -I"C:\Workspaces\DAVE-4.2-64Bit\WS_2015_03_02\EtherCatTest/Libraries/CMSIS/Include" -I"C:\Workspaces\DAVE-4.2-64Bit\WS_2015_03_02\EtherCatTest/Libraries/CMSIS/Infineon/XMC4800_series/Include" -I"C:\Workspaces\DAVE-4.2-64Bit\WS_2015_03_02\EtherCatTest" -I"C:\Workspaces\DAVE-4.2-64Bit\WS_2015_03_02\EtherCatTest\Dave\Generated" -I"C:\Workspaces\DAVE-4.2-64Bit\WS_2015_03_02\EtherCatTest\Libraries" -O0 -ffunction-sections -fdata-sections -Wall -std=gnu99 -mfloat-abi=softfp -Wa,-adhlns="$@.lst" -pipe -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d) $@" -mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mthumb -g -gdwarf-2 -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo.

