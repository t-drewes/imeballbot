#ifndef PRINTFREDIRECT_H_
#define PRINTFREDIRECT_H_

/*
 * Custom printf for printinc via uart, same syntax as normal printf.
 */
int myPrintf(const char* string, ...);

#endif
