/*
 * PrintfRedirect.c
 *
 *  Created on: Jun 2, 2016
 *      Author: Morten Mey
 */
#include"PrintfRedirect.h"
#include<DAVE.h>
#include<string.h>
#include<stdio.h>
#include<stdarg.h>

/*
 * Should also work for normal printf.... (normal printf does not work)
 */
int _write(int file, char *buf, int nbytes)
{
	UART_STATUS_t result = UART_Transmit(&UART_0, (uint8_t*) buf, nbytes);
	/*for(int i = 0; i < nbytes; i++)
	{
		if(USIC_IsTxFIFOfull(UART001_Handle0.UartRegs)) // If the UART Tx fifo is full,
			return i; // Give up and report how many bytes we did send.
		else
			UART001_WriteData(UART001_Handle0,*buf++); // If the Tx fifo is not full, then go ahead and write the byte.
	}*/
	return result == UART_STATUS_SUCCESS ? nbytes : 0;
}

int myPrintf(const char* string, ...){
	static char buf[256];
	va_list va;
	va_start(va, string);
	vsnprintf(buf, 256, string, va);
	va_end(va);
	return _write(0, buf, strlen(buf));
}
