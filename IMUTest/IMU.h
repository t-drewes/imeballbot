/*
 * IMU.h
 *
 *  Created on: Jun 2, 2016
 *      Author: Morten Mey
 */

#ifndef IMU_H_
#define IMU_H_

#include "IMUDefines.h"
#include <stdint.h>

/*
 * Reads a register via i2c.
 * If doStop is true sends a stop signal, default value = true.
 */
uint8_t readI2CReg(uint32_t address, uint8_t reg, bool doStop = true);

/*
 * Writes a register via i2c.
 */
void writeI2CReg(uint32_t address, uint8_t reg, uint8_t data);

/*
 * States for interrupt based i2c communication.
 *  Currently not used.
 */
enum IRQState{
	IDLE,
	READ_STATUS_W,
	READ_STATUS_R,
	READ_COUNT_H_W,
	READ_COUNT_H_R,
	READ_COUNT_L_W,
	READ_COUNT_L_R,
	READ_FIFO_W,
	READ_FIFO_R,
	READ_TEMP_H_W,
	READ_TEMP_H_R,
	READ_TEMP_L_W,
	READ_TEMP_L_R,
	READ_TEMP_DONE
};

class IMU {
public:
	/*
	 * Resets the imu and then configures it.
	 * Between reset and configuration there must be a 100ms pause,
	 * therefore this creates a timer and returns BEFORE the imu is confiugred,
	 * check isValid to see if results have arrived.
	 * Can be configured to use a different address.
	 */
	IMU(const uint32_t address = MPU6050_DEFAULT_ADDRESS);

	/*
	 * Handles the imu interrupt, reading the values via i2c.
	 */
	void handleInterrupt();

	/*
	 * Handles the i2c interrupts, if i2c interrupts are used.
	 * Currently not working.
	 */
	void handleI2cInterrupt(uint8_t receive);

	/*
	 * The gyro values for the three axes.
	 */
	int16_t gyroX, gyroY, gyroZ;
	/*
	 * The accelerometer values for the tree axes.
	 */
	int16_t accelX, accelY, accelZ;
	/*
	 * The temperature in degrees celsius.
	 */
	double temp;

	uint32_t getAddress(){
		return address;
	}

	/*
	 * Returns true if the gyro and accelerometer values are valid.
	 */
	bool isValid(){
		return valid;
	}

	/*
	 * The timer used for configuration. Not for general use.
	 */
	uint32_t getTimerId(){
		return timerId;
	}

	/*
	 * Call to request the temperature be updated in the next interrupt.
	 */
	void requestTempUpdate(){
		readTemp = true;
	}

	/*
	 * True if the temperature has been updated.
	 */
	bool isTempReady(){
		return !readTemp;
	}

private:
	/*
	 * Reads count values from the imus fifo into the buffer buf.
	 */
	void readFifo(uint8_t* buf, uint16_t count);
	const uint32_t address;
	bool valid = false;
	uint32_t timerId;
	IRQState irqState = IDLE;
	bool readTemp = false;
};

#endif /* IMU_H_ */
