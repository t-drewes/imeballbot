/*
 * IMU.cpp
 *
 *  Created on: Jun 2, 2016
 *      Author: Morten Mey
 */

#include "IMU.h"
#include <DAVE.h>
#include "PrintfRedirect.h"

uint8_t readI2CReg(uint32_t address, uint8_t reg, bool doStop) {
	//LSB = Read/Write bit so shift address up by one
	address = address << 1;
	I2C_MASTER_Transmit(&I2C_MASTER_0, true, address, &reg, 1, false);
	while (I2C_MASTER_IsTxBusy(&I2C_MASTER_0));

	uint8_t result;
	I2C_MASTER_Receive(&I2C_MASTER_0, true, address, &result, 1, doStop, true);
	//if(doStop){
		while (I2C_MASTER_IsRxBusy(&I2C_MASTER_0));
	//}
	return result;
}
void writeI2CReg(uint32_t address, uint8_t reg, uint8_t data) {
	address = address << 1;
	uint8_t toSend[2];
	toSend[0] = reg;
	toSend[1] = data;

	I2C_MASTER_Transmit(&I2C_MASTER_0, true, address, toSend, 2, true);
	while (I2C_MASTER_IsTxBusy(&I2C_MASTER_0))
		;
}

static void configureCallback(void* data){
	uint32_t address = ((IMU*)data)->getAddress();
	SYSTIMER_DeleteTimer(((IMU*)data)->getTimerId()); //cleanup timer
	writeI2CReg(address, MPU6050_RA_PWR_MGMT_1, 0); //wakeup imu using internal osci
	writeI2CReg(address, MPU6050_RA_SMPLRT_DIV, 15); // sample rate ~ 450hz
	writeI2CReg(address, MPU6050_RA_GYRO_CONFIG, (1 << 3)); // full scale Range = +-500deg/s
	writeI2CReg(address, MPU6050_RA_ACCEL_CONFIG, (1 << 3)); //full scale Range = +-4g
	//put gyros and accelerometer data in fifo
	writeI2CReg(address, MPU6050_RA_FIFO_EN, (1 << MPU6050_XG_FIFO_EN_BIT) | (1 << MPU6050_YG_FIFO_EN_BIT)
			| (1 << MPU6050_ZG_FIFO_EN_BIT) | (1 << MPU6050_ACCEL_FIFO_EN_BIT));

	//interrupt on new data and fifo full
	writeI2CReg(address, MPU6050_RA_INT_PIN_CFG, (1 << MPU6050_INTCFG_INT_LEVEL_BIT) | (1 << MPU6050_INTCFG_LATCH_INT_EN_BIT));
	//enable temperature and set clock reference to be gyro z
	writeI2CReg(address, MPU6050_RA_PWR_MGMT_1, 3); // temp_dis = 0 => temperature sensor enabled, PLL= Gyro z as reference

	// reset fifo and enable it
	writeI2CReg(address, MPU6050_RA_USER_CTRL, 0);
	writeI2CReg(address, MPU6050_RA_USER_CTRL, (1 << MPU6050_USERCTRL_FIFO_RESET_BIT));
	writeI2CReg(address, MPU6050_RA_USER_CTRL, (1 << MPU6050_USERCTRL_FIFO_EN_BIT));

	//enable interrupts
	writeI2CReg(address, MPU6050_RA_INT_ENABLE, (1 << MPU6050_INTERRUPT_FIFO_OFLOW_BIT) | (1 << MPU6050_INTERRUPT_DATA_RDY_BIT));
}

IMU::IMU(const uint32_t address) : address(address) {
	//reset imu
	writeI2CReg(address, MPU6050_RA_PWR_MGMT_1, (1 << MPU6050_PWR1_DEVICE_RESET_BIT));

	//wait 100 ms
	timerId = SYSTIMER_CreateTimer(100 * 1000, SYSTIMER_MODE_ONE_SHOT, configureCallback, this);
	SYSTIMER_StartTimer(timerId);
}

void IMU::readFifo(uint8_t* buf, uint16_t count){
	const uint32_t add =  address << 1;
	uint8_t reg = MPU6050_RA_FIFO_R_W;
	I2C_MASTER_Transmit(&I2C_MASTER_0, true, add, &reg, 1, false);
	while (I2C_MASTER_IsTxBusy(&I2C_MASTER_0));

	I2C_MASTER_Receive(&I2C_MASTER_0, true, add, buf, count, true, true);
	while (I2C_MASTER_IsRxBusy(&I2C_MASTER_0));
}

//6 values a 16bit
#define DATA_SIZE (6 * 2)
//enough for one data set
#define BUFF_SIZE (1 * DATA_SIZE)

//parses 2 bytes from the buffer ptr starting at idx into one int16
#define PARSE(ptr, idx) ((((int16_t)ptr[idx]) << 8)|(ptr[idx+1]))


void IMU::handleInterrupt(){

	//for i2c interrupt mode
	//irqState = READ_STATUS_W;
	//handleI2cInterrupt(0);
	//comment everything after this for interrupt mode

	//int status lesen
	uint8_t status = readI2CReg(address, MPU6050_RA_INT_STATUS);
	if(status & (1 << MPU6050_INTERRUPT_FIFO_OFLOW_BIT)){
		myPrintf("FIFO OVERFLOWED!");
		//TODO handle fifo overflow
	}
	if(status & (1 << MPU6050_INTERRUPT_DATA_RDY_BIT)){
		//fifo count lesen
		uint16_t count = ((uint16_t)readI2CReg(address, MPU6050_RA_FIFO_COUNTH, false)) << 8;
		count |= readI2CReg(address, MPU6050_RA_FIFO_COUNTL);

		//if fifo contains a full data set read fifo_rw
		if(count >= BUFF_SIZE){
			uint8_t buf[BUFF_SIZE];
			readFifo(buf, BUFF_SIZE);

			//values order in fifo: accelX, accelY, accelZ, temp, gyroX, gyroY, gyroZ
			//only those values specified during configuration will be in the fifo
			accelX = PARSE(buf, 0);
			accelY = PARSE(buf, 2);
			accelZ = PARSE(buf, 4);

			//now temp_h, temp_l if it is in fifo
			//int16_t temperature = PARSE(buf, 6);

			gyroX = PARSE(buf, 6);
			gyroY = PARSE(buf, 8);
			gyroZ = PARSE(buf, 10);
			valid = true;
		}
	}

	if(readTemp){
		int16_t temperature = readI2CReg(address, MPU6050_RA_TEMP_OUT_H, false) << 8;
		temperature |= readI2CReg(address, MPU6050_RA_TEMP_OUT_L);
		//formula taken from data sheet
		temp = temperature / 340.0 + 36.53;
		readTemp  = false;
	}
}

void IMU::handleI2cInterrupt(uint8_t receive){
	//state machine for i2c via interrupt
	//basically does the same as IMU::handleInterrupt() but over multiple calls.
/*
	static uint8_t buf[BUFF_SIZE];
	static uint16_t count;
	static uint8_t reg;

	int16_t temperature;

	switch(irqState){
	case IDLE:
		return;
	case READ_STATUS_W:
		reg = MPU6050_RA_INT_STATUS;
		I2C_MASTER_Transmit(&I2C_MASTER_0, true, address << 1, &reg, 1, true);
		irqState = READ_STATUS_R;
		break;
	case READ_STATUS_R:
		I2C_MASTER_Receive(&I2C_MASTER_0, true, address << 1, buf, 1, true, true);
		irqState = READ_COUNT_H_W;
		break;
	case READ_COUNT_H_W:
		if(buf[0] & (1 << MPU6050_INTERRUPT_FIFO_OFLOW_BIT)){
				myPrintf("FIFO OVERFLOWED!");
				//TODO handle fifo overflow
		}
		if(!(buf[0] & (1 << MPU6050_INTERRUPT_DATA_RDY_BIT))){
			irqState = IDLE;
			return;
		}
		reg = MPU6050_RA_FIFO_COUNTH;
		I2C_MASTER_Transmit(&I2C_MASTER_0, true, address << 1, &reg, 1, false);
		irqState = READ_COUNT_H_R;
		break;
	case READ_COUNT_H_R:
		I2C_MASTER_Receive(&I2C_MASTER_0, true, address << 1, buf, 1, true, true);
		irqState = READ_COUNT_L_W;
		break;
	case READ_COUNT_L_W:
		reg = MPU6050_RA_FIFO_COUNTL;
		I2C_MASTER_Transmit(&I2C_MASTER_0, true, address << 1, &reg, 1, false);
		irqState = READ_COUNT_L_R;
		break;
	case READ_COUNT_L_R:
		I2C_MASTER_Receive(&I2C_MASTER_0, true, address << 1, buf+1, 1, true, true);
		irqState = READ_FIFO_W;
		break;
	case READ_FIFO_W:
		count = buf[0] | (((uint16_t)buf[1])<<8);
		if(count < BUFF_SIZE){
			irqState = IDLE;
		}else{
			reg = MPU6050_RA_FIFO_R_W;
			I2C_MASTER_Transmit(&I2C_MASTER_0, true, address << 1, &reg, 1, false);
			irqState = READ_FIFO_R;
		}
		break;
	case READ_FIFO_R:
		I2C_MASTER_Receive(&I2C_MASTER_0, true, address << 1, buf, BUFF_SIZE, true, true);
		irqState = READ_TEMP_H_W;
		break;
	case READ_TEMP_H_W:
		accelX = PARSE(buf, 0);
		accelY = PARSE(buf, 2);
		accelZ = PARSE(buf, 4);

		//now temp_h, temp_l if it is in fifo

		gyroX = PARSE(buf, 6);
		gyroY = PARSE(buf, 8);
		gyroZ = PARSE(buf, 10);
		valid = true;
		if(readTemp){
			reg = MPU6050_RA_TEMP_OUT_H;
			I2C_MASTER_Transmit(&I2C_MASTER_0, true, address << 1, &reg, 1, false);
			irqState = READ_TEMP_H_R;
		}else{
			irqState = IDLE;
		}
		break;
	case READ_TEMP_H_R:
		I2C_MASTER_Receive(&I2C_MASTER_0, true, address << 1, buf, 1, true, true);
		irqState = READ_TEMP_L_W;
		break;
	case READ_TEMP_L_W:
		reg = MPU6050_RA_TEMP_OUT_L;
		I2C_MASTER_Transmit(&I2C_MASTER_0, true, address << 1, &reg, 1, false);
		irqState = READ_TEMP_L_R;
		break;
	case READ_TEMP_L_R:
		I2C_MASTER_Receive(&I2C_MASTER_0, true, address << 1, buf+1, 1, true, true);
		irqState = READ_TEMP_DONE;
		break;
	case READ_TEMP_DONE:
		temperature = buf[0] | (((uint16_t)buf[1])<<8);
		temp = temperature / 340.0 + 36.53;

		readTemp = false;
		irqState = IDLE;
		break;
	}*/
}

