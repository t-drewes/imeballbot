/*
 * MotorDriverMC33926.h
 *
 *  Created on: Apr 28, 2016
 *      Author: Morten Mey
 */

#ifndef MOTORDRIVERMC33926_H_
#define MOTORDRIVERMC33926_H_

#include "DAVE.h"

#define ND2_PORT XMC_GPIO_PORT1
#define ND2_PIN 8
#define M1DIR_PORT XMC_GPIO_PORT1
#define M1DIR_PIN 9
#define M2DIR_PORT XMC_GPIO_PORT1
#define M2DIR_PIN 10
#define M1PWM_PORT XMC_GPIO_PORT1
#define M1PWM_PIN 11
#define M2PWM_PORT XMC_GPIO_PORT3
#define M2PWM_PIN 10
#define NSF_PORT XMC_GPIO_PORT3
#define NSF_PIN 7
#define M1FB_PORT XMC_GPIO_PORT14
#define M1FB_PIN 0
#define M2FB_PORT XMC_GPIO_PORT14
#define M2FB_PIN 1

#define M1PWM PWM3
#define M2PWM PWM4

//Changing the PWM pins requires rewriting the code!
/*#define M1PWM_CCU CCU80
#define M1PWM_OUT 11
#define M2PWM_CCU CCU41
#define M2PWM_OUT 0
*/
#define M1ADC ADCO
#define M2ADC ADC1

void initDriver();

void setSpeedM1(int32_t speed);
void setSpeedM2(int32_t speed);
void setSpeeds(int32_t m1, int32_t m2);

unsigned int getM1CurrentMilliAmps();
unsigned int getM2CurrentmilliAmps();
unsigned char getDriverFault();

#endif /* MOTORDRIVERMC33926_H_ */
