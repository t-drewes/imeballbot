/*
 * EGPIO.h
 *
 *  Created on: Apr 28, 2016
 *      Author: Morten Mey
 */

#ifndef EGPIO_H_
#define EGPIO_H_

namespace ImeBallBot{

#include "GPIO.h"
#include <assert.h>


template<typename PORT_TYPE>
__STATIC_INLINE void setMode(PORT_TYPE* port, uint8_t pin, uint8_t mode){
	assert(pin < 16);
	switch(pin){
		case 0:
		case 1:
		case 2:
		case 3:
			port->IOCR0 &= ~(0xf8UL << (pin * 8));
			port->IOCR0 |= mode << (pin * 8);
			break;
		case 4:
		case 5:
		case 6:
		case 7:
			pin-=4;
			port->IOCR4 &= ~(0xf8UL << (pin * 8));
			port->IOCR4 |= mode << (pin * 8);
			break;
		case 8:
		case 9:
		case 10:
		case 11:
			pin-=8;
			port->IOCR8 &= ~(0xf8UL << (pin * 8));
			port->IOCR8 |= mode << (pin * 8);
			break;
		case 12:
		case 13:
		case 14:
		case 15:
			pin-=12;
			port->IOCR12 &= ~(0xf8UL << (pin * 8));
			port->IOCR12 |= mode << (pin * 8);
			break;
	}
}

template<typename PORT_TYPE>
__STATIC_INLINE void setDriverStrength(PORT_TYPE* port, uint8_t pin, uint8_t strength){
	assert(pin < 16);
	if(pin < 8){
	    port->PDR0 &= ~(0x7UL << (pin * 4));
	    port->PDR0 |= strength << (pin * 4);
	}else{
		pin -= 8;
		port->PDR1 &= ~(0x7UL << (pin * 4));
		port->PDR1 |= strength << (pin * 4);
	}
}

template<typename PORT_TYPE>
__STATIC_INLINE void setHwsel(PORT_TYPE* port, uint8_t pin, uint32_t config){
    assert(pin < 16);
	port->HWSEL &= ~(0x3UL << (pin * 2));
    port->HWSEL |= config << (pin * 2);
}

template<typename PORT_TYPE>
__STATIC_INLINE void set(PORT_TYPE* port, uint8_t pin){
	assert(pin < 16);
	port->OMR = 1UL << pin;
}

template<typename PORT_TYPE>
__STATIC_INLINE void reset(PORT_TYPE* port, uint8_t pin){
	assert(pin < 16);
    port->OMR = 1UL << (pin + 16);
}

template<typename PORT_TYPE>
__STATIC_INLINE void toggle(PORT_TYPE* port, uint8_t pin){
	assert(pin < 16);
    port->OMR = 1UL << pin | 1UL << (pin + 16);
}

template<typename PORT_TYPE>
__STATIC_INLINE uint32_t read(PORT_TYPE* port, uint8_t pin){
	assert(pin < 16);
    return(port->IN & (1UL << pin));
}

}

#endif /* EGPIO_H_ */
