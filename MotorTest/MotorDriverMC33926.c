/*
 * MotorDriverMC.cpp
 *
 *  Created on: Apr 28, 2016
 *      Author: Morten Mey
 */

#include "MotorDriverMC33926.h"

void initDriver(){

	XMC_GPIO_CONFIG_t dirConfig = {XMC_GPIO_MODE_OUTPUT_PUSH_PULL,
			XMC_GPIO_OUTPUT_LEVEL_LOW, XMC_GPIO_OUTPUT_STRENGTH_STRONG_SOFT_EDGE};
	XMC_GPIO_Init(M1DIR_PORT, M1DIR_PIN, &dirConfig);
	XMC_GPIO_Init(M2DIR_PORT, M2DIR_PIN, &dirConfig);

	XMC_GPIO_CONFIG_t m2PwmPinConfig = {XMC_GPIO_MODE_OUTPUT_PUSH_PULL_ALT3,
				XMC_GPIO_OUTPUT_LEVEL_LOW, XMC_GPIO_OUTPUT_STRENGTH_STRONG_SOFT_EDGE};
	XMC_GPIO_Init(M2PWM_PORT, M2PWM_PIN, &m2PwmPinConfig);
	//reset CCU4
	/*SCU_RESET->PRSET0 |= SCU_RESET_PRSET0_CCU41RS_Msk;
	SCU_RESET->PRCLR0 |= SCU_RESET_PRCLR0_CCU41RS_Msk;

	//activate CCU clock
	SCU_CLK->CLKSET |= SCU_CLK_CLKSET_CCUCEN_Msk;

	//configure CCU41
	CCU41->GIDLC |= CCU4_GIDLC_SPRB_Msk;
	//CCU41->GCTRL = 0; //is reset default
	//CCU41_CC40->INS = 0;
	//CCU41_CC40->CMC = 0;
	//CCU41_CC40->TC = 0;
	//CCU41_CC40->PSL = 0;
	CCU41_CC40->PSC = 15; // for CLK=144MHz
	CCU41_CC40->PR = 479;
	CCU41_CC40->CR = 240;

	CCU41_CC40->TCSET |= CCU4_CC4_TCSET_TRBS_Msk; //start timer
	CCU41->GIDLC |= CCU4_GIDLC_CS0I_Msk;
	*/
	XMC_GPIO_CONFIG_t nsfPinConfig = {XMC_GPIO_MODE_INPUT_PULL_UP,
			XMC_GPIO_OUTPUT_LEVEL_LOW, XMC_GPIO_OUTPUT_STRENGTH_STRONG_SOFT_EDGE};
	XMC_GPIO_Init(NSF_PORT, NSF_PIN, &nsfPinConfig);

	XMC_GPIO_CONFIG_t nd2PinConfig = {XMC_GPIO_MODE_OUTPUT_PUSH_PULL,
			XMC_GPIO_OUTPUT_LEVEL_HIGH, XMC_GPIO_OUTPUT_STRENGTH_STRONG_SOFT_EDGE};
	XMC_GPIO_Init(ND2_PORT, ND2_PIN, &nd2PinConfig);
}

void setSpeedM1(int32_t speed){
	char reverse;
	if(speed < 0){
		speed = -speed;
		reverse = true;
	}else{
		reverse = false;
	}

	//DO PWM

	if(reverse){
		XMC_GPIO_SetOutputHigh(M1DIR_PORT, M1DIR_PIN);
	}else{
		XMC_GPIO_SetOutputLow(M1DIR_PORT, M1DIR_PIN);
	}

}
void setSpeedM2(int32_t speed){
	char reverse;
	if(speed < 0){
		speed = -speed;
		reverse = true;
	}else{
		reverse = false;
	}

	PWM_CCU4_SetDutyCycle(&PWM_CCU4_0, speed);

	if(reverse){
		XMC_GPIO_SetOutputHigh(M2DIR_PORT, M2DIR_PIN);
	}else{
		XMC_GPIO_SetOutputLow(M2DIR_PORT, M2DIR_PIN);
	}
}
void setSpeeds(int32_t m1, int32_t m2){
	setSpeedM1(m1);
	setSpeedM2(m2);
}

unsigned int getM1CurrentMilliAmps(){

}
unsigned int getM2CurrentmilliAmps(){

}
unsigned char getDriverFault(){
	return !XMC_GPIO_GetInput(NSF_PORT, NSF_PIN);
}
