\section{Simulation basierend auf 2D-Modell}

Der Ballbot ist ein inhärent instabiles System, weshalb der Umsetzung einer zuverlässigen Regelung eine besondere Rolle zukommt. In Anlehnung an das Vorgehen in der Bachelorarbeit von Frankhauser und Gwerder der ETH Zürich\footnote{Bachelor-Thesis: Modeling and Control of a Ballbot, ETH Zürich, 2010} wurde das 3-dimensionale System zunächst in drei voneinander unabhängige planare Modelle überführt, die jeweils die Bewegung in der XY-, YZ- und XZ-Ebene beschreiben. Dabei besteht jedes Modell aus einem Ball, einem antreibenden Omniwheel und dem restlichen Aufbau. Die Körper werden auf die Ebene projeziert.

Die Zustände des Systems sind die Orientierungswinkel des Aufbaus sowie die Position und Geschwindigkeit des Balls. Da alle diese Zustände von der IMU und den Motor-Encodern erfasst werden, entsprechen die Ausgaben des Systems den Zuständen. Da die Zustände in jeder Ebene einzeln betrachtet werden und der Ballbot sich nicht in z-Richtung bewegen kann, besitzen die Rotation des Aufbaus und die Translation/Rotation des Balls nur \textit{einen} Freiheitsgrad. (siehe Abbildung 2.1 im Zürich-Paper). Die Eingaben des Modells sind die vom Motor verursachten Drehmomente.

Um das System zu beschreiben, werden die Koordinaten wie in Abbildung 2.2 im Zürich-Paper definiert. $\theta_{x,y,z}$ beschreibt die Orientierung des Aufbaus in der jeweiligen Ebene. Weiterhin beschreiben $\phi_{x,y,z}$ die Orientierung des Balls und $\psi_{x,y,z}$ die Winkel der virtuellen Omniwheels. Dabei gilt, dass $\theta_x$ den Winkel in der YZ-Ebene angibt (analog gilt ähnliches für die anderen Variablen).

Der MATLAB-Code und die Simulink-Implementierung befinden sich im Ordner \texttt{/simu\-lation/\allowbreak model-\allowbreak 2d-\allowbreak zuerich} im Git-Repository. Um die Simulationen (\texttt{Simulation\-With\-Control}, \texttt{Simulation\-Without\-Control}) ausführen zu können, muss zuvor das Skript \texttt{Constants.m} ausgeführt werden. Dieses lädt wichtige Parameter des Ballbots (Massen, Radien, Winkel), die für die Simulation benötigt werden, in den MATLAB-Workspace.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{images/simulation2d.png}
\caption{Implementierung der 2D-Simulation}
\label{fig:implementation_2d_simulation}
\end{figure}

\subsection{Bewegungsgleichungen}

Die Bewegungsgleichungen für die XY-Ebene waren bereits vorgegeben (siehe Zürich, S. 12, Gleichungen (2.25) und (2.26)). Jene für die XZ- und YZ-Ebene wurden im Ansatz beschrieben und mussten selbst berechnet werden. Dies wurde im Skript \texttt{Equations\-Of\-Motion\-Calculation.m} implementiert.

\subsection{Komponenten der Simulink-Implementierung}

\subsubsection*{PlanarModel}

In dieser Komponente erhält jede Ebene des Modells ein Drehmoment als Eingabe, setzt diesen in die Bewegungsgleichungen ein und liefert den neuen Zustand des Systems in dieser Ebene als Ausgabe. Hier ist gut sichtbar, dass alle Ebenen voneinander unabhängig betrachtet werden.

\subsubsection*{PlanarModelXYPlane, PlanarModelXZPlane, PlanarModelYZPlane}

In den f(u)-Blöcken lässt sich ein beliebiger auszuwertender Ausdruck eintragen. Hier finden die Bewegungsgleichungen ihren Platz. Der Mux erhält von oben nach unten die Einträge $\theta_x, \dot\theta_x $ und $\ddot\theta_x$. Die Rückführung der integrierten Resultate ist notwendig, um die Differentialgleichung zu lösen.

\subsubsection*{Controller}

Der Baustein besteht im Wesentlichen aus je einem PID-Regler je Ebene. Eingabe sind die Differenzen zwischen Soll- und Ist-Wert. Ausgabe sind die Drehmomente für die einzelnen Motoren.

\subsubsection*{DiffBlock}

Diese Komponente berechnet den Unterschied zwischen Soll- und Ist-Wert. Die Eingabe für den Sollwert ist in der Simulation ein Sprung von $0^\circ$ auf $10^\circ$ nach 1 Sekunde Simulationszeit. Die Eingabe für den Ist-Wert wird rückgeführt aus dem \textit{PlanarModel}.

\subsection{Einstellung des PID-Reglers}

Bei der Suche nach geeigneten Regel-Parametern haben wir uns für die empirische Methode entschieden. Dabei wird zu Beginn der Einstellung ein geringer Wert für $K_P$ gewählt und die Werte für $K_I$ und $K_D$ auf 0 gesetzt. $K_P$ wird nun so lange erhöht, bis das System deutlich schwingt. Anschließend wird $K_D$-Anteil schrittweise erhöht, bis das System nicht mehr schwingt. Um den bleibenden Regel-Fehler zu minimieren und das Gesamtergebnis zu optimieren, wird ein geringer Wert für $K_I$ gesetzt.

\subsection{Evaluation der Simulation}

Im folgenden Diagramm ist die Abweichung von Soll- und Ist-Wert dargestellt, also der Scope nach dem DiffBlock. Nach 1 Sekunde erfolgt ein Sprung von $0^\circ$ auf $10^\circ$ auf jeder Ebene. Die rote Linie stellt die Abweichung auf XY-Ebene dar; die blaue und die gelbe Linie die Abweichungen auf der XZ- bzw. YZ-Ebene. Erwartungsgemäß verhalten sich beide Ebenen äquivalent, da das Modell die selben Bewegungsgleichungen für diese Ebenen verwendet. Etwa 1 Sekunde (XY-Ebene) bzw. 2 Sekunden (XZ-/YZ-Ebene) nach dem Sprung ist die Abweichung ausgeregelt.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{images/sim2d-diff-scope-less.png}
\caption{Ausführung der 2D-Simulation: Differenz von Soll- und Ist-Werten}
\label{fig:execution_2d_simulation}
\end{figure}

